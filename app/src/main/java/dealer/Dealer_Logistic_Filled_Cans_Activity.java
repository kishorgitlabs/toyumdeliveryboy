package dealer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import logout.logout;

public class Dealer_Logistic_Filled_Cans_Activity extends AppCompatActivity {

    TextView DealerName,DealerCode,Source,Filledcans,Date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer_logistic_filled_cans);

        DealerName = (TextView)findViewById(R.id.name);
        DealerCode = (TextView)findViewById(R.id.code);
        Source = (TextView)findViewById(R.id.source);
        Filledcans = (TextView)findViewById(R.id.filledcans);
        Date = (TextView)findViewById(R.id.date);


    }

    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }
    public void Log_Out(View view) {
        new logout(this).log_out();
    }
}
