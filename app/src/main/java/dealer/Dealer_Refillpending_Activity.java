package dealer;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.HorizontalScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.Maps_Activity;
import com.brainmagic.toyumwater.R;
import com.google.android.gms.common.api.CommonStatusCodes;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.TransformerUtils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import apiservice.APIService;
import adapter.PendingCansadapter;
import alertbox.Alert;
import alertbox.AlertDialogue;
import butterknife.BindView;
import butterknife.ButterKnife;
import customedittext.CustomEditText;
import customedittext.DrawableClickListener;
import logout.logout;

import model.Dealer.PendingcanList.Datum;
import model.Dealer.PendingcanList.PendingCansList;
import retroclient.RetroClient;
import sharedpreference.Shared;
import toaster.Toasts;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Dealer_Refillpending_Activity extends AppCompatActivity {
    @BindView(R.id.fromdate)
    CustomEditText Fromdate;
    @BindView(R.id.todate)
    CustomEditText Todate;
    TextView DealerName;
    private String S_FromDate, S_ToDate;
    private Date Datefrom, Dateto;
    Alert alert = new Alert(this);
    NetworkConnection network = new NetworkConnection(this);
    Toasts Toaster = new Toasts(this);
    private RecyclerView recyclerView;
    private List<Datum> PendingCansList;
    private HorizontalScrollView Horizontalscrollview;
    private SharedPreferences myshare;
    Toasts toasts = new Toasts(this);
    private static final int PERMISSION_REQUEST = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer_refillpending);
        ButterKnife.bind(this);
        DealerName = (TextView) findViewById(R.id.dealername);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        Horizontalscrollview = (HorizontalScrollView) findViewById(R.id.HorizontalScrollView);
        PendingCansList = new ArrayList<>();
        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);

        DealerName.setText(myshare.getString(Shared.K_Name, ""));

        Fromdate.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable text) {
                // TODO Auto-generated method stub
                if (text.length() > 0) {
                    try {
                        Fromdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_cancel, 0);
                    } catch (Exception ex) {
                        Log.v("afterTextChanged", ex.getMessage());
                    }
                }
            }
        });
        Todate.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable text) {
                // TODO Auto-generated method stub
                if (text.length() > 0) {
                    try {
                        Todate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_cancel, 0);
                    } catch (Exception ex) {
                        Log.v("afterTextChanged", ex.getMessage());
                    }
                }
            }
        });
        Fromdate.setDrawableClickListener(new DrawableClickListener() {


            public void onClick(DrawablePosition target) {
                switch (target) {
                    case LEFT:
                        //Do something here
                        break;

                    case RIGHT:
                        Fromdate.getText().clear();
                        Fromdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        break;
                    default:
                        break;
                }
            }

        });


        Todate.setDrawableClickListener(new DrawableClickListener() {


            public void onClick(DrawablePosition target) {
                switch (target) {
                    case LEFT:
                        //Do something here
                        break;

                    case RIGHT:
                        Todate.getText().clear();
                        Todate.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        break;
                    default:
                        break;
                }
            }

        });
        Fromdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Calendar now = Calendar.getInstance();
                final Calendar c = Calendar.getInstance();

                DatePickerDialog dpd = new DatePickerDialog(Dealer_Refillpending_Activity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            DecimalFormat mFormat = new DecimalFormat("00");

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                c.set(year, monthOfYear, dayOfMonth);
                                Datefrom = c.getTime();

                                String myFormat = "yyyy-MM-dd"; //In which you need put here
                                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                S_FromDate = sdf.format(c.getTime());
                                Log.v("S_ChequeDate", S_FromDate);
//yyyy-MM-dd
                                String myFormat2 = "dd/MM/yyyy"; //In which you need put here
                                SimpleDateFormat sdf2 = new SimpleDateFormat(myFormat2, Locale.US);
                                Fromdate.setText(sdf2.format(c.getTime()));
                                Log.v("ChequeDate", Fromdate.getText().toString());

                        /*    PurchaseDate.setText(mFormat.format(dayOfMonth) + "/" + mFormat.format(monthOfYear + 1) + "/" + year);
                            S_Date  = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
*/


                            }
                        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                dpd.show();
            }
        });
        Todate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Calendar now = Calendar.getInstance();
                if (Datefrom != null) {
                    final Calendar c = Calendar.getInstance();

                    final DatePickerDialog dpd = new DatePickerDialog(Dealer_Refillpending_Activity.this,
                            new DatePickerDialog.OnDateSetListener() {
                                /*    DecimalFormat mFormat= new DecimalFormat("00");*/
                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    c.set(year, monthOfYear, dayOfMonth);
                                    Dateto = c.getTime();

                                    String myFormat = "yyyy-MM-dd"; //In which you need put here
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                    S_ToDate = sdf.format(c.getTime());

                                    Log.v("S_ChequeDate", S_ToDate);
//yyyy-MM-dd
                                    String myFormat2 = "dd/MM/yyyy"; //In which you need put here
                                    SimpleDateFormat sdf2 = new SimpleDateFormat(myFormat2, Locale.US);

                                    Todate.setText(sdf2.format(c.getTime()));
                                    Log.v("ChequeDate", Todate.getText().toString());

                            /*PurchaseDate.setText(mFormat.format(dayOfMonth) + "/" + mFormat.format(monthOfYear + 1) + "/" + year);
                            S_Date  = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;*/


                                }
                            }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                    dpd.getDatePicker().setMinDate(Datefrom.getTime());
                    dpd.show();
                } else {
                    toasts.ShowErrorToast("Select From date");
                }
            }
        });


        if (network.CheckInternet()) {
            Get_PendingList("", "");
        } else {
            alert.showAlertbox(getString(R.string.no_network));
        }

        /*  List<String> list = new ArrayList<String>();
        list.add("select");
        list.add("Ashok Pillar");
        list.add("Guindy");
        list.add("Liberty");
        list.add("Mount Road");
        list.add("Vadapalani");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Areaspinner.setAdapter(dataAdapter);*/
    }

    public void Search(View view) {
        if (Fromdate.getText().toString().equals(""))
            toasts.ShowErrorToast("Select From date");
        else if (Todate.getText().toString().equals(""))
            toasts.ShowErrorToast("Select To date");
        else if (network.CheckInternet())
            Get_PendingList(S_FromDate, S_ToDate);
        else
            alert.showAlertbox(getString(R.string.no_network));

    }


    private void Get_PendingList(String Fromdate, String Todate) {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        APIService service = RetroClient.getApiService();


        Call<PendingCansList> call = service.PENDING_CANS_LIST_CALL(myshare.getString(Shared.K_Code, ""), Fromdate, Todate);
        call.enqueue(new Callback<PendingCansList>() {
            @Override
            public void onResponse(Call<PendingCansList> call, Response<PendingCansList> response) {
                progressDialog.dismiss();
                switch (response.body().getResult()) {
                    case "Success": {
                        PendingCansList = response.body().getData();
                        PendingCansadapter mAdapter = new PendingCansadapter(Dealer_Refillpending_Activity.this, PendingCansList);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(mAdapter);
                        break;
                    }
                    case "No data": {
                        //alert.showAlertbox("No Installations found");
                        AlertDialogue alertnew = new AlertDialogue(Dealer_Refillpending_Activity.this);
                        Horizontalscrollview.setVisibility(View.GONE);
                        alertnew.showAlertbox("No Refill data found");
                        alertnew.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                finish();
                            }
                        });
                        //alert.
                        //Body_layout.setVisibility(View.GONE);
                        break;
                    }
                    default: {

                        alert.showAlertbox(getString(R.string.connection_slow));
                        Horizontalscrollview.setVisibility(View.GONE);
                        //Body_layout.setVisibility(View.GONE);
                        break;
                    }
                }
            }

            @Override
            public void onFailure(Call<PendingCansList> call, Throwable t) {
                progressDialog.dismiss();
                alert.showAlertbox(getString(R.string.connection_slow));
                Horizontalscrollview.setVisibility(View.GONE);

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    //   Barcode barcode = data.getParcelableExtra("barcode");

                    String BarcodeValue = data.getStringExtra("barcode");
                    int position = data.getIntExtra("position", 0);
                    //String barcodevalue =
                    //Toast.makeText(this, "Barcode : "+barcode.displayValue+"\n"+position, Toast.LENGTH_LONG).show();
                    Log.v("Barcode Value", BarcodeValue);
                    Log.v("Customer Code", PendingCansList.get(position).getCustomerCode());
                    if (BarcodeValue.equals(PendingCansList.get(position).getCustomerCode())) {

                        if (PendingCansList.get(position).getRefillStatus().equals("Partially Delivered")) {


                            Intent a = new Intent(Dealer_Refillpending_Activity.this, Dealer_Delivery_Confirmation_Activity.class)
                                    .putExtra("id", PendingCansList.get(position).getId())
                                    .putExtra("code", PendingCansList.get(position).getCustomerCode())
                                    .putExtra("name", PendingCansList.get(position).getCustomerName())
                                    .putExtra("refill", PendingCansList.get(position).getRefillCans() - PendingCansList.get(position).getRefillCansDeliverd())
                                    .putExtra("qrcode", "Yes");

                            //a.putExtra("Serial id",barcode.displayValue);

                            startActivity(a);
                            //holder.Text3.setText(String.format("%s", (PendingCansList.get(listPosition).getRefillCans() - PendingCansList.get(listPosition).getRefillCansDeliverd())));
                        } else {
                            //holder.Text3.setText(String.format("%s", PendingCansList.get(listPosition).getRefillCans()));
                            Intent a = new Intent(Dealer_Refillpending_Activity.this, Dealer_Delivery_Confirmation_Activity.class)
                                    .putExtra("id", PendingCansList.get(position).getId())
                                    .putExtra("code", PendingCansList.get(position).getCustomerCode())
                                    .putExtra("name", PendingCansList.get(position).getCustomerName())
                                    .putExtra("refill", PendingCansList.get(position).getRefillCans())
                                    .putExtra("qrcode", "Yes");

                            //a.putExtra("Serial id",barcode.displayValue);

                            startActivity(a);
                        }


                        //Barcodevale.setText("Barcode : "+barcode.displayValue );
                    } else {
                        Toaster.ShowErrorToast("QRCode does not match Customer details");
                    }
                } else {
                    Toast.makeText(this, "Scanning failed! Please scan again.", Toast.LENGTH_LONG).show();

                    //  Barcodevale.setText("No value Found" );
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Dealer_Welcome_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }

    public void Log_Out(View view) {
        new logout(this).log_out();
    }

    public void Popup_Menu(View view) {
    }

    @Override
    protected void onRestart() {
        recreate();
        super.onRestart();
    }

    public void View_Map(View view) {


        if (network.CheckInternet()) {
            GetPermission();
            //repaireIdList = (List<Object>) CollectionUtils.collect(repairedlistData, TransformerUtils.invokerTransformer("getId"));

        } else
            alert.showAlertbox(getString(R.string.no_network));
    }

void Start_Mapactivity(){
    ArrayList<Datum> ArrayList = new ArrayList<Datum>(PendingCansList);
    //ArrayList<Object> latlist =new ArrayList<>();
    //latlist = (ArrayList<Object>) CollectionUtils.collect(PendingCansList, TransformerUtils.invokerTransformer("getId"));
    //datalist.addAll(Arrays.asList(PendingCansList.));
    startActivity(new Intent(Dealer_Refillpending_Activity.this, Maps_Activity.class).putExtra("pendinglist", ArrayList));
}


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void GetPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Snackbar.make(findViewById(R.id.dealerhome), "You need give permission", Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void onClick(View v) {
                            //your action here
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST);
                        }
                    }).show();

                } else {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST);
                }
            } else {
                Start_Mapactivity();
            }
        } else

        {
            Start_Mapactivity();
        }

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            Snackbar.make(findViewById(R.id.dealerhome), "Permission Granted",
                    Snackbar.LENGTH_LONG).show();
            Start_Mapactivity();

        } else {

            Snackbar.make(findViewById(R.id.dealerhome), "Permission denied",
                    Snackbar.LENGTH_LONG).show();

        }
    }


}
