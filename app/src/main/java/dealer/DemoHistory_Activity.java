package dealer;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import adapter.DemoHistoryadapter;
import apiservice.APIService;
import adapter.InstallationHistoryadapter;
import alertbox.Alert;
import alertbox.AlertDialogue;
import butterknife.BindView;
import butterknife.ButterKnife;
import customedittext.CustomEditText;
import customedittext.DrawableClickListener;
import logout.logout;

import model.Dealer.demopendinginstalls.Datum;
import model.Dealer.demopendinginstalls.DemoPendings;
import retroclient.RetroClient;
import sharedpreference.Shared;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toaster.Toasts;

public class DemoHistory_Activity extends AppCompatActivity {

            @BindView(R.id.fromdate)
            CustomEditText Fromdate;
            @BindView(R.id.todate)
            CustomEditText Todate;

            private String S_FromDate,S_ToDate;
            private Date Datefrom,Dateto;
            Alert alert = new Alert(this);
            NetworkConnection network = new NetworkConnection(this);
            Toasts toasts = new Toasts(this);
            ImageView Back,Home;
            TextView Customer_Name;

            private SharedPreferences myshare;
            private SharedPreferences.Editor editor;
            private RecyclerView recyclerView;
            private List<Datum> DemoHistoryList;
            private HorizontalScrollView Horizontalscrollview;

            String S_Fromdate = "",S_Todate= "";
            final Calendar c_from = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_history);
                ButterKnife.bind(this);
                Back = (ImageView)findViewById(R.id.back);
                Home = (ImageView)findViewById(R.id.home);
                Customer_Name = (TextView)findViewById(R.id.name);
                recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
                Horizontalscrollview = (HorizontalScrollView) findViewById(R.id.HorizontalScrollView);
                DemoHistoryList = new ArrayList<>();

                myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
                editor = myshare.edit();

                Customer_Name.setText(myshare.getString(Shared.K_Name,""));

                Fromdate.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count,
                                                  int after) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void afterTextChanged(Editable text) {
                        // TODO Auto-generated method stub
                        if (text.length() > 0) {
                            try {
                                Fromdate.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_action_cancel,0);
                            } catch (Exception ex) {
                                Log.v("afterTextChanged", ex.getMessage());
                            }
                        }
                    }
                });
                Todate.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count,
                                                  int after) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void afterTextChanged(Editable text) {
                        // TODO Auto-generated method stub
                        if (text.length() > 0) {
                            try {
                                Todate.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_action_cancel,0);
                            } catch (Exception ex) {
                                Log.v("afterTextChanged", ex.getMessage());
                            }
                        }
                    }
                });
                Fromdate.setDrawableClickListener(new DrawableClickListener() {


                    public void onClick(DrawablePosition target) {
                        switch (target) {
                            case LEFT:
                                //Do something here
                                break;

                            case RIGHT:
                                Fromdate.getText().clear();
                                Fromdate.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                                break;
                            default:
                                break;
                        }
                    }

                });


                Todate.setDrawableClickListener(new DrawableClickListener() {


                    public void onClick(DrawablePosition target) {
                        switch (target) {
                            case LEFT:
                                //Do something here
                                break;

                            case RIGHT:
                                Todate.getText().clear();
                                Todate.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                                break;
                            default:
                                break;
                        }
                    }

                });
                Fromdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Calendar now = Calendar.getInstance();
                        final Calendar c = Calendar.getInstance();

                        DatePickerDialog dpd = new DatePickerDialog(DemoHistory_Activity.this,
                                new DatePickerDialog.OnDateSetListener() {
                                    DecimalFormat mFormat= new DecimalFormat("00");
                                    @Override
                                    public void onDateSet(DatePicker view, int year,
                                                          int monthOfYear, int dayOfMonth) {
                                        c.set(year, monthOfYear, dayOfMonth);
                                        Datefrom = c.getTime();

                                        String myFormat = "yyyy-MM-dd"; //In which you need put here
                                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                        S_FromDate = sdf.format(c.getTime());
                                        Log.v("S_ChequeDate", S_FromDate);
//yyyy-MM-dd
                                        String myFormat2 = "dd/MM/yyyy"; //In which you need put here
                                        SimpleDateFormat sdf2 = new SimpleDateFormat(myFormat2, Locale.US);
                                        Fromdate.setText(sdf2.format(c.getTime()));
                                        Log.v("ChequeDate", Fromdate.getText().toString());

                        /*    PurchaseDate.setText(mFormat.format(dayOfMonth) + "/" + mFormat.format(monthOfYear + 1) + "/" + year);
                            S_Date  = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
*/


                                    }
                                }, c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DATE));
                        dpd.show();
                    }
                });
                Todate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Calendar now = Calendar.getInstance();
                        if(Datefrom != null){
                            final Calendar c = Calendar.getInstance();

                            final DatePickerDialog dpd = new DatePickerDialog(DemoHistory_Activity.this,
                                    new DatePickerDialog.OnDateSetListener() {
                                        /*    DecimalFormat mFormat= new DecimalFormat("00");*/
                                        @Override
                                        public void onDateSet(DatePicker view, int year,
                                                              int monthOfYear, int dayOfMonth) {
                                            c.set(year, monthOfYear, dayOfMonth);
                                            Dateto = c.getTime();

                                            String myFormat = "yyyy-MM-dd"; //In which you need put here
                                            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                            S_ToDate = sdf.format(c.getTime());

                                            Log.v("S_ChequeDate", S_ToDate);
//yyyy-MM-dd
                                            String myFormat2 = "dd/MM/yyyy"; //In which you need put here
                                            SimpleDateFormat sdf2 = new SimpleDateFormat(myFormat2, Locale.US);

                                            Todate.setText(sdf2.format(c.getTime()));
                                            Log.v("ChequeDate", Todate.getText().toString());

                            /*PurchaseDate.setText(mFormat.format(dayOfMonth) + "/" + mFormat.format(monthOfYear + 1) + "/" + year);
                            S_Date  = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;*/



                                        }
                                    }, c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DATE));
                            dpd.getDatePicker().setMinDate(Datefrom.getTime());
                            dpd.show();
                        }else{
                            toasts.ShowErrorToast("Select From date");
                        }
                    }
                });


                if(network.CheckInternet())
                {
                    Get_Installation_History("","");
                }else{
                    alert.showAlertbox(getString(R.string.no_network));
                }

                Back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
                Home.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
                        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(a);
                    }
                });


            }
            public void Search(View view) {
                if(Fromdate.getText().toString().equals(""))
                    toasts.ShowErrorToast("Select From date");
                else if(Todate.getText().toString().equals(""))
                    toasts.ShowErrorToast("Select To date");
                else if(network.CheckInternet())
                    Get_Installation_History(S_FromDate,S_ToDate);
                else
                    alert.showAlertbox(getString(R.string.no_network));

            }
            public void Log_Out(View view) {
                new logout(this).log_out();
            }

            public void Popup_Menu(View view) {
            }

            private void Get_Installation_History(String From, String To){

                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Loading...");
                progressDialog.show();
                APIService service = RetroClient.getApiService();

                Call<DemoPendings> call = service.DEMO_HISTORY_CALL(myshare.getString(Shared.K_Code,""),From,To);
                call.enqueue(new Callback<DemoPendings>() {
                    @Override
                    public void onResponse(Call<DemoPendings> call, Response<DemoPendings> response) {
                        progressDialog.dismiss();
                        switch (response.body().getResult()) {
                            case "Success":
                            {
                                DemoHistoryList = response.body().getData();
                                DemoHistoryadapter mAdapter = new DemoHistoryadapter(DemoHistory_Activity.this, DemoHistoryList);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                recyclerView.setLayoutManager(mLayoutManager);
                                recyclerView.setItemAnimator(new DefaultItemAnimator());
                                recyclerView.setAdapter(mAdapter);
                                break;
                            }
                            case "No data":
                            {
                                //alert.showAlertbox("No Installations found");
                                AlertDialogue alertnew = new AlertDialogue(DemoHistory_Activity.this);
                                Horizontalscrollview.setVisibility(View.GONE);
                                alertnew.showAlertbox("Dear Dealer, your installation list is empty");
                                alertnew.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        finish();
                                    }
                                });
                                //alert.
                                //Body_layout.setVisibility(View.GONE);
                                break;
                            }
                            default: {

                                alert.showAlertbox(getString(R.string.connection_slow));
                                Horizontalscrollview.setVisibility(View.GONE);
                                //Body_layout.setVisibility(View.GONE);
                                break;
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<DemoPendings> call, Throwable t) {
                        progressDialog.dismiss();
                        alert.showAlertbox(getString(R.string.connection_slow));
                        Horizontalscrollview.setVisibility(View.GONE);

                    }
                });

            }
            private String StringToDate(String Date)
            {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
                SimpleDateFormat timeformat = new SimpleDateFormat("HH:mm:ss");
                SimpleDateFormat newtimeformat = new SimpleDateFormat("hh:mm a");

                java.util.Date d = null;
                String newdate = null;
                try {
                    d = sdf.parse(Date.substring(0,10));
                    newdate = sdf2.format(d);
                } catch (ParseException ex) {
                    Log.v("Date Exception",ex.getMessage());
                }
                Date time = null;
                String newtime = null;
                try {
                    time = timeformat.parse(Date.substring(11,19));
                    newtime = newtimeformat.format(time);
                } catch (ParseException ex) {
                    Log.v("Date Exception",ex.getMessage());
                }


                return newdate + "\n"+newtime;
            }

            public void From_Date(View view) {


                DatePickerDialog dpd = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                Fromdate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                S_Fromdate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                            }
                        }, c_from.get(Calendar.YEAR),c_from.get(Calendar.MONTH),c_from.get(Calendar.DATE));
                dpd.show();
            }

            public void To_Date(View view) { final Calendar c = Calendar.getInstance();

                DatePickerDialog dpd = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                Todate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                S_Todate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                            }
                        }, c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DATE));
                dpd.getDatePicker().setMinDate(c_from.getTimeInMillis());
                dpd.show();
            }


        }
