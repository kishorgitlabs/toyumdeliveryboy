package dealer;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import java.util.ArrayList;
import java.util.List;

import apiservice.APIService;
import adapter.CustomerListadapter;
import alertbox.Alert;
import alertbox.AlertDialogue;
import logout.logout;
import model.Dealer.CustomerList.CustomerListDelaer;

import model.Dealer.CustomerList.Datum;
import model.customerlist.Cu;
import model.customerlist.CustomerList;
import retroclient.RetroClient;
import sharedpreference.Shared;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Dealer_CustomerList_Activity extends AppCompatActivity {
    ImageView Back,Home;
    TextView Customer_Name,Customer_Code;
    String cumstomerMobileNO;
    Alert alert = new Alert(this);
    NetworkConnection network = new NetworkConnection(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private RecyclerView recyclerView;
//    private List<Datum> OrderHistoryList;
private List<Cu> OrderHistoryList;
    private HorizontalScrollView Horizontalscrollview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer_customerlist);

        Back = (ImageView)findViewById(R.id.back);
        Home = (ImageView)findViewById(R.id.home);
        Customer_Name = (TextView)findViewById(R.id.name);
        Customer_Code = (TextView)findViewById(R.id.code);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        Horizontalscrollview = (HorizontalScrollView) findViewById(R.id.HorizontalScrollView);
        OrderHistoryList = new ArrayList<>();
        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();

        Customer_Name.setText(myshare.getString(Shared.K_Name,""));
        Customer_Code.setText(myshare.getString(Shared.K_Code,""));
        cumstomerMobileNO = myshare.getString("", "");

        if(network.CheckInternet())
        {
            Get_OrderHistory();
        }else{
            alert.showAlertbox(getString(R.string.no_network));
        }

        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(getApplicationContext(), Dealer_Welcome_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });


    }
    public void Log_Out(View view) {
        new logout(this).log_out();
    }

    public void Popup_Menu(View view) {
    }

    private void Get_OrderHistory(){

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        APIService service = RetroClient.getApiService();

//        Call<CustomerListDelaer> call = service.CUSTOMER_LIST_DELAER_CALL(myshare.getString(Shared.K_Code,"")
//        , myshare.getString(Shared.K_RegisterMobile,""));

        Call<CustomerList> call = service.NEW_CUSTOMER_LIST_DELAER_CALL(myshare.getString(Shared.K_Code,""));
        call.enqueue(new Callback<CustomerList>() {
            @Override
            public void onResponse(Call<CustomerList> call, Response<CustomerList> response) {
                progressDialog.dismiss();
                switch (response.body().getResult()) {
                    case "Success":
                    {
                        OrderHistoryList = response.body().getData().getCu();
                        CustomerListadapter mAdapter = new CustomerListadapter(Dealer_CustomerList_Activity.this,
                                OrderHistoryList, response.body().getData().getRg());
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(mAdapter);
                        break;
                    }
                    case "No data":
                    {
                        //alert.showAlertbox("No Installations found");
                        AlertDialogue alertnew = new AlertDialogue(Dealer_CustomerList_Activity.this);
                        Horizontalscrollview.setVisibility(View.GONE);
                        alertnew.showAlertbox("Dear Dealer, your Customer list is empty");
                        alertnew.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                finish();
                            }
                        });
                        //alert.
                        //Body_layout.setVisibility(View.GONE);
                        break;
                    }
                    default: {

                        alert.showAlertbox(getString(R.string.connection_slow));
                        Horizontalscrollview.setVisibility(View.GONE);
                        //Body_layout.setVisibility(View.GONE);
                        break;
                    }
                }
            }

            @Override
            public void onFailure(Call<CustomerList> call, Throwable t) {
                progressDialog.dismiss();
                alert.showAlertbox(getString(R.string.connection_slow));
                Horizontalscrollview.setVisibility(View.GONE);
            }
        });
    }
}