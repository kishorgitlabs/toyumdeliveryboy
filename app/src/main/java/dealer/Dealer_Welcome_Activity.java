package dealer;

import android.Manifest;
import android.support.design.widget.Snackbar;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import java.util.List;

import alertbox.Alert;
import logout.logout;
import model.Dealer.CustomerList.Datum;
import sharedpreference.Shared;
import network.NetworkConnection;

public class Dealer_Welcome_Activity extends AppCompatActivity {
   // RelativeLayout Refill_layout,Installation_layout,Delivery_layout;
    ImageView Back,Home;

    TextView Customer_Name,Customer_Code;
    Alert alert = new Alert(this);
    NetworkConnection network = new NetworkConnection(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    RelativeLayout installation;
    List<Datum> datas;

    private static final int PERMISSION_REQUEST = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer_welcome);

        Back = (ImageView)findViewById(R.id.back);
        Home = (ImageView)findViewById(R.id.home);
        installation=findViewById(R.id.installation);
        Customer_Name = (TextView)findViewById(R.id.name);
        Customer_Code = (TextView)findViewById(R.id.code);

        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();
        Customer_Name.setText("  "+myshare.getString(Shared.K_Name,""));
        Customer_Code.setText(myshare.getString(Shared.K_Code,""));

                Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(getApplicationContext(), Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });


    }

    public void Water_Refill_Pending(View view) {
        startActivity(new Intent(getApplicationContext(),Dealer_Refillpending_Activity.class));
    }
     @RequiresApi(api = Build.VERSION_CODES.M)
     public void Installation_List(View view) {
         if(network.CheckInternet())
         {
             String activity = "installation";
//             GetPermission(activity);
             startActivity(new Intent(getApplicationContext(), Dealer_Installation_Activity.class));
         }

         else{
             alert.showAlertbox(getString(R.string.no_network));
         }
        // installation startActivity(new Intent(getApplicationContext(),Dealer_Installation_Activity.class));
    }

    public void Customer_Delivery_Confirmation(View view) {
        startActivity(new Intent(getApplicationContext(),Dealer_Delivery_Activity.class));
    }
    public void Change_Password(View view) {
        startActivity(new Intent(getApplicationContext(),Dealer_Change_Password_Activity.class));
    }

    public void Logistics_Management(View view) {
        startActivity(new Intent(getApplicationContext(),Dealer_Logistic_Activity.class));
    }

    public void Customer_Location_Update(View view) {

        if(network.CheckInternet())
        {
            String activity = "location";
//            GetPermission(activity);
            startActivity(new Intent(getApplicationContext(), Dealer_Installation_Activity.class));
        }else{
            alert.showAlertbox(getString(R.string.no_network));
        }
        //startActivity(new Intent(getApplicationContext(),Dealer_Customer_Location_Activity.class));
    }

    public void Log_Out(View view) {
        new logout(this).log_out();
    }

    public void Delivery_History(View view) {

        startActivity(new Intent(getApplicationContext(),Dealer_Delivery_History_Activity.class));

    }

    public void Installation_History(View view) {
        startActivity(new Intent(getApplicationContext(),Dealer_Installation_History_Activity.class));
    }

    public void View_Customer_List(View view) {
        startActivity(new Intent(getApplicationContext(),Dealer_CustomerList_Activity.class));
    }




    @RequiresApi(api = Build.VERSION_CODES.M)
    private void GetPermission(String Activity_name) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Snackbar.make(findViewById(R.id.dealerhome), "You need give permission", Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void onClick(View v) {
                            //your action here
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST);
                        }
                    }).show();

                } else {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST);
                }
            } else {
                TrackLocation(Activity_name);
            }
        } else

        {
            TrackLocation(Activity_name);
        }

    }



    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            Snackbar.make(findViewById(R.id.dealerhome), "Permission Granted",
                    Snackbar.LENGTH_LONG).show();
            //TrackLocation();

        } else {

            Snackbar.make(findViewById(R.id.dealerhome), "Permission denied",
                    Snackbar.LENGTH_LONG).show();

        }
    }

void TrackLocation(String Activity_name){
    switch (Activity_name) {
        case "location":
            Select_Options();
            //   startActivity(new Intent(getApplicationContext(),Dealer_Customer_Location_Activity.class));
            break;
        case "installation":

//            startActivity(new Intent(getApplicationContext(), Dealer_Installation_Activity.class));
            break;
    }


}

    private void Select_Options() {



        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.select_customer_options, null);
        alertDialog.setView(dialogView);


        Button  Cancel = (Button) dialogView.findViewById(R.id.cancel);
        Button Deposit_Update = (Button) dialogView.findViewById(R.id.deposit);
        Button Location_Update = (Button) dialogView.findViewById(R.id.location);



        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        Location_Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                startActivity(new Intent(getApplicationContext(),Dealer_Customer_Location_Activity.class));
            }
        });
        Deposit_Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Dealer_Customer_Deposit_Activity.class));
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void Demo_Installations(View view) {
        startActivity(new Intent(getApplicationContext(), Demo_Install_Activity.class));
    }

    public void Demo_History(View view) {
        startActivity(new Intent(getApplicationContext(), DemoHistory_Activity.class));
    }
}
