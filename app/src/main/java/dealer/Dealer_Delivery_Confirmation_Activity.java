package dealer;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import java.text.DateFormat;
import java.util.Date;

import apiservice.APIService;
import alertbox.Alert;
import alertbox.AlertDialogue;
import hidekeyboard.HideSoftKeyboard;
import logout.logout;
import model.Dealer.DeliveryUpdate.DeliveryConfimation;
import retroclient.RetroClient;
import sharedpreference.Shared;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Dealer_Delivery_Confirmation_Activity extends AppCompatActivity {
    TextView CustomerName, CustomerCode, Refillcans, QRCode, Current_Date, CustomerAddress;
    EditText Emptycans, DeliveredCans;
    long Id;
    String Name, Code, Refill, QRcode, Address;
    Alert alert = new Alert(this);
    NetworkConnection network = new NetworkConnection(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer_delivery_confirmation);
        new HideSoftKeyboard().setupUI(findViewById(R.id.dealer_delivery_confirmation), this);
        CustomerName = (TextView) findViewById(R.id.name);
        CustomerCode = (TextView) findViewById(R.id.code);
        Refillcans = (TextView) findViewById(R.id.filledcans);
        QRCode = (TextView) findViewById(R.id.qrcode);
        Current_Date = (TextView) findViewById(R.id.date);
        Emptycans = (EditText) findViewById(R.id.emptycans);
        DeliveredCans = (EditText) findViewById(R.id.deliveredcans);
        CustomerAddress = (TextView) findViewById(R.id.customer_address);

        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();

        //DealerName.setText(String.format("Dealer : %s", myshare.getString(Shared.K_Name, "")));

        Id = getIntent().getLongExtra("id", 0);

        Log.v("id", String.format("%s", Id));
        Name = getIntent().getStringExtra("name");
        Code = getIntent().getStringExtra("code");
        Address = getIntent().getStringExtra("customerAddress");
        CustomerAddress.setText(Address);

        QRcode = getIntent().getStringExtra("qrcode");
        Refill = String.format("%s", getIntent().getLongExtra("refill", 0));
        CustomerName.setText(Name);
        SpannableString content = new SpannableString(Code);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);

        CustomerCode.setText(content);
        Refillcans.setText(Refill);
        QRCode.setText(QRcode);

        String currentDateTimeString = DateFormat.getDateInstance().format(new Date());
        Current_Date.setText(currentDateTimeString);

    }

    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Dealer_Welcome_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }


    public void Log_Out(View view) {
        new logout(this).log_out();
    }


    public void Confirm(View view) {

        if (DeliveredCans.getText().toString().equals("")) {
            DeliveredCans.setError("Enter empty cans");
            DeliveredCans.setFocusable(true);
        } else if (Emptycans.getText().toString().equals("")) {
            Emptycans.setError("Enter empty cans");
            Emptycans.setFocusable(true);
        } else if (network.CheckInternet()) {
            Confirm_delivery();
        } else {
            alert.showAlertbox(getString(R.string.no_network));
        }
    }

    public void Popup_Menu(View view) {
    }

    public void Confirm_delivery() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        APIService service = RetroClient.getApiService();

        Call<DeliveryConfimation> call = service.DELIVERY_CONFIMATION_CALL((int) Id,
                Refill,
                Emptycans.getText().toString(),
                QRcode,
                DeliveredCans.getText().toString());
        call.enqueue(new Callback<DeliveryConfimation>() {
            @Override
            public void onResponse(Call<DeliveryConfimation> call, Response<DeliveryConfimation> response) {
                progressDialog.dismiss();
                switch (response.body().getResult()) {
                    case "Success": {

                        AlertDialogue alertnew = new AlertDialogue(Dealer_Delivery_Confirmation_Activity.this);
                        alertnew.showAlertbox("Delivery Confirmation updated Successfully");
                        alertnew.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                //startActivity(new Intent(getApplicationContext(),Dealer_Installation_Activity.class));
                                finish();
                                /*Intent a = new Intent(getApplicationContext(), Home_Activity.class);
                                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(a);*/

                            }
                        });
                        //finish();
                        break;
                    }
                    case "Already Updated": {
                        alert.showAlertbox("Confirmation already updated! Please check");

                        break;
                    }

                    case "error":{
                        alert.showAlertbox("Confirmation already updated! Please check !");

                        break;
                    }


                    default: {

                        alert.showAlertbox(getString(R.string.connection_slow));

                        break;
                    }
                }
            }

            @Override
            public void onFailure(Call<DeliveryConfimation> call, Throwable t) {
                progressDialog.dismiss();
                alert.showAlertbox(getString(R.string.connection_slow));
                //Horizontalscrollview.setVisibility(View.GONE);

            }
        });
    }


}
