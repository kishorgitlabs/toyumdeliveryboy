package dealer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import alertbox.Alert;
import logout.logout;
import sharedpreference.Shared;
import network.NetworkConnection;

public class Dealer_Delivery_Activity extends AppCompatActivity {


        TextView CustomerName,CustomerCode,DealerName;
    long Id,Refill;
    String Name,Code;
    Alert alert = new Alert(this);
    NetworkConnection network = new NetworkConnection(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer_delivery);

        CustomerName = (TextView) findViewById(R.id.customername);
        CustomerCode = (TextView) findViewById(R.id.customercode);
        DealerName = (TextView) findViewById(R.id.name);

        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();
        //DealerName.setText(String.format("Dealer : %s", myshare.getString(Shared.K_Name, "")));
        DealerName.setText(myshare.getString(Shared.K_Name, ""));

        Id = getIntent().getLongExtra("id",0);

        Log.v("id",String.format("%s",Id));
        Name = getIntent().getStringExtra("name");
        Code = getIntent().getStringExtra("code");
        Refill = getIntent().getLongExtra("refill",0);

        CustomerName.setText(Name);
        CustomerCode.setText(Code);

    }

    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }


    public void Log_Out(View view) {
        new logout(this).log_out();
    }

    public void Popup_Menu(View view) {
    }

    public void Confirm_Manually(View view) {
        startActivity(new Intent(getApplicationContext(), Dealer_Delivery_Confirmation_Activity.class)
                .putExtra("id",Id)
                .putExtra("code",Code)
                .putExtra("name",Name)
                .putExtra("refill",Refill)
                .putExtra("qrcode","No")
        );

    }

    public void Confirm_QRcode(View view) {
        startActivity(new Intent(getApplicationContext(), Dealer_Delivery_Confirmation_Activity.class)
                .putExtra("id",Id)
                .putExtra("code",Code)
                .putExtra("name",Name)
                .putExtra("refill",Refill)
                .putExtra("qrcode","Yes")
        );

    }
}
