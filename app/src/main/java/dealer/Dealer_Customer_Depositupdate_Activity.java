package dealer;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import apiservice.APIService;
import alertbox.Alert;
import alertbox.AlertDialogue;
import hidekeyboard.HideSoftKeyboard;
import logout.logout;
import model.Dealer.CustomerDepositUpdate.DepositUpdate;
import retroclient.RetroClient;
import sharedpreference.Shared;
import toaster.Toasts;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Dealer_Customer_Depositupdate_Activity extends AppCompatActivity {
    private MaterialSpinner PaymentTypespinner;
    private Spinner Bankspinner;
    private TextView CustomerName, CustomerCode, DepositAmount, DepositAmountPaid;
    private EditText Amount, ChequeNumber, ChequeDate;
    private Button Update, Cancel;
    private LinearLayout Row_PaymentType, Row_Bank, Row_ChequeNumber, Row_ChequeDate, Row_Amount;
    long Deposit, DepositPaid;
    Alert alert = new Alert(this);
    NetworkConnection network = new NetworkConnection(this);
    Toasts toaster = new Toasts(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    String S_Name, S_Code, S_Paymenttype, S_Bank, S_ChequeNumber, S_ChequeDate, S_Amount;
    Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer_customer_depositeupdate);
        new HideSoftKeyboard().setupUI(findViewById(R.id.dealer_customer_depositupdate),this);
        PaymentTypespinner = (MaterialSpinner) findViewById(R.id.paymenttype);
        Bankspinner = (Spinner) findViewById(R.id.bank_spinner);
        CustomerName = (TextView) findViewById(R.id.name);
        CustomerCode = (TextView) findViewById(R.id.code);
        DepositAmount = (TextView) findViewById(R.id.deposit);
        DepositAmountPaid = (TextView) findViewById(R.id.depositpaid);

        Row_PaymentType = (LinearLayout) findViewById(R.id.l_paymenttype);
        Row_Bank = (LinearLayout) findViewById(R.id.l_bankname);
        Row_ChequeNumber = (LinearLayout) findViewById(R.id.l_chequenumber);
        Row_ChequeDate = (LinearLayout) findViewById(R.id.l_chequedate);
        Row_Amount = (LinearLayout) findViewById(R.id.l_amount);

        Amount = (EditText) findViewById(R.id.amount);
        ChequeNumber = (EditText) findViewById(R.id.chequenumber);
        ChequeDate = (EditText) findViewById(R.id.chequedate);
        Update = (Button) findViewById(R.id.update);


//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.bank_arrays, R.layout.spinner_item);
        //spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
//        Bankspinner.setAdapter(adapter);
        //Bankspinner.setel
        PaymentTypespinner.setItems("Select", "Cash", "Cheque");

        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();


        S_Name = getIntent().getStringExtra("name");
        S_Code = getIntent().getStringExtra("code");
        Deposit = getIntent().getLongExtra("deposit", 0);
        DepositPaid = getIntent().getLongExtra("depositpaid", 0);

        CustomerName.setText(S_Name);
        CustomerCode.setText(S_Code);
        DepositAmount.setText(String.format("%s", getString(R.string.RS) + " " + Deposit + ".00"));
        DepositAmountPaid.setText(String.format("%s", getString(R.string.RS) + " " + DepositPaid + ".00"));

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                //updateLabel();
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                S_ChequeDate = sdf.format(myCalendar.getTime());
                Log.v("S_ChequeDate", S_ChequeDate);
//yyyy-MM-dd
                String myFormat2 = "dd/MM/yyyy"; //In which you need put here
                SimpleDateFormat sdf2 = new SimpleDateFormat(myFormat2, Locale.US);
                ChequeDate.setText(sdf2.format(myCalendar.getTime()));
                Log.v("ChequeDate", ChequeDate.getText().toString());
            }

        };


        ChequeDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(Dealer_Customer_Depositupdate_Activity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        PaymentTypespinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                String payment = item.toString();
                if (payment.equals("Cheque")) {
                    //Row_PaymentType.setVisibility(View.VISIBLE);
                    Row_Bank.setVisibility(View.VISIBLE);
                    Row_ChequeNumber.setVisibility(View.VISIBLE);
                    Row_ChequeDate.setVisibility(View.VISIBLE);
                    Row_Amount.setVisibility(View.VISIBLE);

                } else if (payment.equals("Cash")) {
                    Row_Amount.setVisibility(View.VISIBLE);
                    Row_Bank.setVisibility(View.GONE);
                    Row_ChequeNumber.setVisibility(View.GONE);
                    Row_ChequeDate.setVisibility(View.GONE);
                }
            }
        });

        Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                S_Paymenttype = PaymentTypespinner.getText().toString();
                S_Bank = Bankspinner.getSelectedItem().toString();
                S_ChequeNumber = ChequeNumber.getText().toString();
                S_Amount = Amount.getText().toString();

                if (S_Paymenttype.equals("Select")) {
                    toaster.ShowErrorToast("Please Select PaymentType !");
                } else if (S_Paymenttype.equals("Cash")) {
                    S_Bank = "";
                    S_ChequeDate = "";
                    S_ChequeNumber = "";
                    if (S_Amount.equals("")) {
                        toaster.ShowErrorToast("Please Enter Deposit Amount!");
                    }  else if (Integer.parseInt(S_Amount) - DepositPaid > Deposit) {
                        toaster.ShowErrorToast("Please Enter valid Amount!");
                    } else if (network.CheckInternet()) {
                        //toaster.ShowSuccessToast("Success");
                        UpdateDeposit();
                    } else {
                        alert.showAlertbox(getString(R.string.no_network));
                    }

                } else if (S_Paymenttype.equals("Cheque")) {
                    if (S_Bank.equals("Select")) {
                        toaster.ShowErrorToast("Please Select Bank Name!");
                    } else if (S_ChequeNumber.equals(""))
                        toaster.ShowErrorToast("Please Enter Cheque Number!");
                    else if (S_ChequeNumber.length() < 6) {
                        toaster.ShowErrorToast("Enter valid Cheque Number!");
                    } else if (ChequeDate.getText().toString().equals("")) {
                        toaster.ShowErrorToast("Please Enter Cheque Date!");
                    } else if (S_Amount.equals("")) {
                        toaster.ShowErrorToast("Please Enter Deposit Amount!");
                    } else if (Integer.parseInt(S_Amount) - DepositPaid > Deposit) {
                        toaster.ShowErrorToast("Please Enter valid Amount!");
                    } else if (network.CheckInternet()) {
                        //toaster.ShowSuccessToast("Success");
                        UpdateDeposit();
                    } else {
                        alert.showAlertbox(getString(R.string.no_network));
                    }

                }
            }
        });

    }

    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }


    public void Log_Out(View view) {
        new logout(this).log_out();
    }

    public void Popup_Menu(View view) {
    }

    public void UpdateDeposit() {

        try {
            final ProgressDialog progressDialog = new ProgressDialog(Dealer_Customer_Depositupdate_Activity.this);
            progressDialog.setMessage("Updating...");
            progressDialog.show();
            APIService service = RetroClient.getApiService();

            Call<DepositUpdate> call = service.DEPOSIT_UPDATE_CALL(
                    S_Name,
                    S_Code,
                    "Deposit",
                    S_Paymenttype,
                    S_Amount,
                    S_Bank,
                    S_ChequeNumber,
                    S_ChequeDate,
                    myshare.getString(Shared.K_Code, "")
            );
            Log.v("Code", myshare.getString(Shared.K_Code, ""));
            call.enqueue(new Callback<DepositUpdate>() {
                @Override
                public void onResponse(Call<DepositUpdate> call, Response<DepositUpdate> response) {
                    progressDialog.dismiss();
                    switch (response.body().getResult()) {
                        case "Success": {

                            AlertDialogue alertnew = new AlertDialogue(Dealer_Customer_Depositupdate_Activity.this);
                            alertnew.showAlertbox("Deposit Amount updated Successfully");
                            alertnew.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    finish();
                                }
                            });
                            //finish();
                            break;
                        }
                        case "Failure": {

                            alert.showAlertbox("Error in Updating! Please try again!");
                            break;
                        }
                        case "No data": {

                            alert.showAlertbox("No data found!");
                            break;
                        }
                        default: {

                            alert.showAlertbox(getString(R.string.connection_slow));
                            //Horizontalscrollview.setVisibility(View.GONE);
                            //Body_layout.setVisibility(View.GONE);
                            break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<DepositUpdate> call, Throwable t) {
                    progressDialog.dismiss();
                    alert.showAlertbox(getString(R.string.connection_slow));

                }
            });
        } catch (Exception ex) {
            Log.v("UpdateException", ex.getMessage());
        }
    }
}
