package dealer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import apiservice.APIService;
import alertbox.Alert;
import logout.logout;
import model.ChangePassword.changepassword;
import retroclient.RetroClient;
import sharedpreference.Shared;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Dealer_Change_Password_Activity extends AppCompatActivity {

    TextView Dealer_Name, Dealer_Code;
    EditText CurrentPassword, NewPassword, ConfirmPassword;
    CheckBox showPassword;

    Alert alert = new Alert(this);
    NetworkConnection network = new NetworkConnection(this);

    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer_change_password);

        Dealer_Name = (TextView) findViewById(R.id.name);
        Dealer_Code = (TextView) findViewById(R.id.code);
        CurrentPassword = (EditText) findViewById(R.id.current_password);
        NewPassword = (EditText) findViewById(R.id.new_password);
        ConfirmPassword = (EditText) findViewById(R.id.confirm_password);
        showPassword = (CheckBox) findViewById(R.id.show_password);

        showPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    CurrentPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    NewPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    ConfirmPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }else {
                    CurrentPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    NewPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    ConfirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();

        Dealer_Name.setText(myshare.getString(Shared.K_Name, ""));
        Dealer_Code.setText(myshare.getString(Shared.K_Code, ""));
//        CurrentPassword.setText(myshare.getString(Shared.K_Password, ""));

    }

    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Dealer_Welcome_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }

    public void Change_Password(View view) {
        if (CurrentPassword.getText().toString().length() == 0) {
            CurrentPassword.setFocusable(true);
            CurrentPassword.setError("Enter Password");
        } else if (NewPassword.getText().toString().length() == 0) {
            NewPassword.setFocusable(true);
            NewPassword.setError("Enter Password");
        } else if (ConfirmPassword.getText().toString().length() == 0) {
            ConfirmPassword.setFocusable(true);
            ConfirmPassword.setError("Enter Password");
        } else if (NewPassword.getText().toString().length() < 5) {
            NewPassword.setFocusable(true);
            NewPassword.setError("Password should be minimum 5 characters");
        } else if (!NewPassword.getText().toString().equals(ConfirmPassword.getText().toString())) {
            ConfirmPassword.setFocusable(true);
            ConfirmPassword.setError("New password & Confirm password doesn't match");
        } else {
            if (network.CheckInternet()) {
                Change_Password();
            } else {
                alert.showAlertbox(getString(R.string.no_network));
            }
        }
    }

    private void Change_Password() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        APIService service = RetroClient.getApiService();
        Call<changepassword> call = service.Change_Password(myshare.getLong(Shared.K_Id, 0), CurrentPassword.getText().toString(), NewPassword.getText().toString());

        call.enqueue(new Callback<changepassword>() {
            @Override
            public void onResponse(Call<changepassword> call, Response<changepassword> response) {
                progressDialog.dismiss();
                switch (response.body().getResult()) {
                    case "Success":
                        alert.showAlertbox("Dear Dealer, your new password " + response.body().getData().getNewPassword() + " updated successfully !");
                        CurrentPassword.setText("");
                        NewPassword.setText("");
                        ConfirmPassword.setText("");
                        break;
                    case "IncorrectPassword":
                        alert.showAlertbox("Your Current password is Incorrect! Kindly enter valid password");
                        break;
                    default:
                        alert.showAlertbox(getString(R.string.connection_slow));
                        break;

                }
            }

            @Override
            public void onFailure(Call<changepassword> call, Throwable t) {
                progressDialog.dismiss();
                alert.showAlertbox(getString(R.string.connection_slow));
            }
        });


    }

    public boolean isAlphaNumeric(String s){
        if(s.length() < 0) {
            //Pattern pattern = Pattern.compile("([A-Za-z]+[0-9]|[0-9]+[A-Za-z])[A-Za-z0-9]*");
            Pattern pattern = Pattern.compile("^(?=.*[a-zA-Z])(?=.*\\d)[A-Za-z\\d!@#$%^&*()_+]{7,19}$");
            Matcher m = pattern.matcher(s);
            return m.matches();
        }
        else{
            return false;
        }

    }
    public void Log_Out(View view) {
        new logout(this).log_out();
    }

    public void Popup_Menu(View view) {
    }
}
