package dealer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import java.util.ArrayList;
import java.util.List;

import logout.logout;

public class Dealer_Payment_Activity extends AppCompatActivity {
    Spinner Areaspinner;
    ImageView Back,Home;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer_payment);
        Areaspinner = (Spinner) findViewById(R.id.areaspinner);

        List<String> list = new ArrayList<String>();
        list.add("select");
        list.add("Ashok Pillar");
        list.add("Guindy");
        list.add("Liberty");
        list.add("Mount Road");
        list.add("Vadapalani");

        Back = (ImageView)findViewById(R.id.back);
        Home = (ImageView)findViewById(R.id.home);

        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(getApplicationContext(), Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Areaspinner.setAdapter(dataAdapter);

    }
    public void Log_Out(View view) {
        new logout(this).log_out();
    }
}
