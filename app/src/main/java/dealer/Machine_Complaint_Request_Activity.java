package dealer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import java.util.ArrayList;
import java.util.List;

import logout.logout;

public class Machine_Complaint_Request_Activity extends AppCompatActivity {
    Spinner Saletypespinner;
    Button Register;
    ImageView Back,Home;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_machine_complaint_request);
        Saletypespinner = (Spinner) findViewById(R.id.saletypespinner);
        Register = (Button) findViewById(R.id.register);
        Back = (ImageView)findViewById(R.id.back);
        Home = (ImageView)findViewById(R.id.home);

        List<String> list = new ArrayList<String>();
        list.add("select");
        list.add("Free");
        list.add("Paid");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Saletypespinner.setAdapter(dataAdapter);

        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(getApplicationContext(), Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });

        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Complaint Registered Successfully",Toast.LENGTH_SHORT).show();
            }
        });

    }
    public void Log_Out(View view) {
        new logout(this).log_out();
    }

    public void Popup_Menu(View view) {
    }
}
