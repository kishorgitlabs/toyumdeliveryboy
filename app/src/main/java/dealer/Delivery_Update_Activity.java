package dealer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import logout.logout;

public class Delivery_Update_Activity extends AppCompatActivity {
    Button Search;
    LinearLayout Gridlayout;
    ImageView Back,Home;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_update);

        Search = (Button)findViewById(R.id.search);
        Gridlayout = (LinearLayout) findViewById(R.id.grid_layout);
        Back = (ImageView)findViewById(R.id.back);
        Home = (ImageView)findViewById(R.id.home);

        Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gridlayout.setVisibility(View.VISIBLE);
            }
        });
        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(getApplicationContext(), Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });

    }
    public void Log_Out(View view) {
        new logout(this).log_out();
    }
}