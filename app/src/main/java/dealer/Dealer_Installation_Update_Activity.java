package dealer;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;
import com.brainmagic.toyumwater.Scanning_Activity;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.jaredrummler.materialspinner.MaterialSpinner;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import apiservice.APIService;
import alertbox.Alert;
import alertbox.AlertDialogue;
import hidekeyboard.HideSoftKeyboard;
import logout.logout;
import model.Bank.BankNames;
import model.Dealer.InstallationUpdate.Installationupdate;
import model.installationupdate.InstallationUpdate;
import retroclient.RetroClient;
import sharedpreference.Shared;
import toaster.Toasts;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import service.GPSTracker;

public class Dealer_Installation_Update_Activity extends AppCompatActivity {

    List<String> banklist;
    private MaterialSpinner spinner, Paymentspinner, PaymentTypespinner;
    private Spinner bankspinner;
    private TextView CustomerName, walletamount, CustomerAddress, CustomerCode, DepositAmount, FilledcansRequested, Latitude, Longitude, QRCode, paymentType, canPerRate, totalAmount, canCost;
    private LinearLayout Row_PaymentType, Row_Bank, Row_ChequeNumber, Row_ChequeDate, Row_Amount, paymentTypeCod, swiping_code;

    private EditText DepositAmountPaid, Filledcans, ChequeNumber, ChequeDate, enter_swiping_code;
    private Button Update, Cancel;

    private static final int PERMISSION_REQUEST = 100;

    double latitude; // latitude
    double longitude; // longitude

    long Id;
    double TotalAmount, CanCost, CanPerRate, Deposit;
    CheckBox canCover, pump, canhandle, canopener;
    Alert alert = new Alert(this);
    NetworkConnection network = new NetworkConnection(this);
    Toasts toaster = new Toasts(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    String  filledcan, door_no, filledcanss, area, swpingcodes, Name, mobileno, Code, Address, CusCode, swiping_codes, S_QRCode, S_Payment, S_Paymenttype, S_Bank, PaymentType, S_ChequeNumber, S_ChequeDate, S_Amount, mobile;
    Calendar myCalendar = Calendar.getInstance();
    boolean CanCover = false, Pump = false;
    int cans;
    double getwalletamount;
    double a = 0.00, b = 0.00, c = 0.00, d = 0.00,e=0.00;
    String A, B, C, D;
    int requestacan;
    String totalamounts;
    String wallet;
    int total;
    ImageView home;
    double newAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer_installation_update);

//        setupParent(findViewById(R.id.dealer_installation_update));
        new HideSoftKeyboard().setupUI(findViewById(R.id.dealer_installation_update), this);
        spinner = (MaterialSpinner) findViewById(R.id.qrcodespinner);
        Paymentspinner = (MaterialSpinner) findViewById(R.id.payment);
        PaymentTypespinner = (MaterialSpinner) findViewById(R.id.paymenttype);
        bankspinner = (Spinner) findViewById(R.id.bank_spinner);
        paymentType = (TextView) findViewById(R.id.payment_type);
        CustomerAddress = (TextView) findViewById(R.id.customer_address);
        canhandle = findViewById(R.id.can_handle);
        canopener = findViewById(R.id.can_opener);
        swiping_code = findViewById(R.id.swiping_code);
        enter_swiping_code = findViewById(R.id.enter_swiping_code);
        home = findViewById(R.id.home);
        walletamount = findViewById(R.id.walletamount);

        getbankname();

        spinner.setItems("Select", "Yes", "No");

        Paymentspinner.setItems("Select", "Cash", "Cheque", "Swiping Machine");

        PaymentTypespinner.setItems("Select", "Cash", "Cheque");

        Row_PaymentType = (LinearLayout) findViewById(R.id.l_paymenttype);
        Row_Bank = (LinearLayout) findViewById(R.id.l_bankname);
        Row_ChequeNumber = (LinearLayout) findViewById(R.id.l_chequenumber);
        Row_ChequeDate = (LinearLayout) findViewById(R.id.l_chequedate);
        Row_Amount = (LinearLayout) findViewById(R.id.l_amount);
        paymentTypeCod = (LinearLayout) findViewById(R.id.payment_type_cod);

        CustomerName = (TextView) findViewById(R.id.name);

//        CustomerCode = (TextView) findViewById(R.id.code);

        DepositAmount = (TextView) findViewById(R.id.deposit);
        FilledcansRequested = (TextView) findViewById(R.id.cans);
        Latitude = (TextView) findViewById(R.id.latitude);
        Longitude = (TextView) findViewById(R.id.longitude);
        QRCode = (TextView) findViewById(R.id.qrcode);

        totalAmount = (TextView) findViewById(R.id.total_amount);
        canCost = (TextView) findViewById(R.id.can_cost);
        canPerRate = (TextView) findViewById(R.id.can_cost_per);
        canCover = (CheckBox) findViewById(R.id.can_cover);
        pump = (CheckBox) findViewById(R.id.pump);


        DepositAmountPaid = (EditText) findViewById(R.id.depositpaid);


        ChequeNumber = (EditText) findViewById(R.id.chequenumber);
        ChequeDate = (EditText) findViewById(R.id.chequedate);
        Filledcans = (EditText) findViewById(R.id.filledcans);

        Update = (Button) findViewById(R.id.update);
        Cancel = (Button) findViewById(R.id.cancel);

        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();


        editor.commit();
        Id = getIntent().getLongExtra("id", 0);

        Log.v("id", String.format("%s", Id));
        Name = getIntent().getStringExtra("name");
        cans = getIntent().getIntExtra("cans", 1);

        requestacan = getIntent().getIntExtra("cans", 1);
        mobile = getIntent().getStringExtra("mobileNo");
        CanPerRate = getIntent().getDoubleExtra("canPerCost", 0);
        Deposit = getIntent().getDoubleExtra("depositCost", 0);
        CanCost = getIntent().getDoubleExtra("canCost", 0);
        CusCode = getIntent().getStringExtra("customerCode");
        Address = getIntent().getStringExtra("customerAddress");
        getwalletamount = getIntent().getDoubleExtra("walletamount", 0);

        filledcanss = String.valueOf(cans);
        door_no = getIntent().getStringExtra("customerAddress");

        CustomerAddress.setText(door_no);
        CustomerName.setText(mobile);

        TotalAmount = getIntent().getDoubleExtra("deposit", 0);


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(getApplicationContext(), Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });


        if (getIntent().getStringExtra("paymentType").equals("Razorpay")) {
            paymentType.setText("Online");
        } else {
            paymentType.setText(getIntent().getStringExtra("paymentType"));
        }
        CustomerName.setText(Name + "," + mobile);
//        CustomerCode.setText(Code);
        SpannableString content = new SpannableString(mobile);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);


        DepositAmount.setText(String.format("%s", getString(R.string.RS) + " " + Deposit));
        canCost.setText(String.format("%s", getString(R.string.RS) + " " + CanCost));
//        totalAmount.setText(String.format("%s", getString(R.string.RS) + TotalAmount)+"0");
        walletamount.setText(String.format("%s", getString(R.string.RS) + getwalletamount)+"0");
        canPerRate.setText(String.format("%s", getString(R.string.RS) + CanPerRate));

        c = getwalletamount +Deposit+CanCost;
        e=Deposit+CanCost;


        totalAmount.setText(String.format("%s", getString(R.string.RS) + c)+"0");

        String totalValue = String.valueOf(c);
        DepositAmountPaid.setText(c+"0");

        FilledcansRequested.setText(filledcanss);

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                //updateLabel();
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                S_ChequeDate = sdf.format(myCalendar.getTime());
                Log.v("S_ChequeDate", S_ChequeDate);
//yyyy-MM-dd
                String myFormat2 = "dd/MM/yyyy"; //In which you need put here
                SimpleDateFormat sdf2 = new SimpleDateFormat(myFormat2, Locale.US);
                ChequeDate.setText(sdf2.format(myCalendar.getTime()));
                Log.v("ChequeDate", ChequeDate.getText().toString());
            }

        };


        ChequeDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(Dealer_Installation_Update_Activity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        if (paymentType.getText().toString().equals("COD")) {
            paymentTypeCod.setVisibility(View.VISIBLE);

//            Row_Amount.setVisibility(View.VISIBLE);
        } else {
//            Row_Amount.setVisibility(View.GONE);
        }

        Paymentspinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                String payment = item.toString();
                if (payment.equals("Cheque")) {
                    //Row_PaymentType.setVisibility(View.VISIBLE);
                    Row_Bank.setVisibility(View.VISIBLE);
                    Row_ChequeNumber.setVisibility(View.VISIBLE);
                    Row_ChequeDate.setVisibility(View.VISIBLE);
                    Row_Amount.setVisibility(View.VISIBLE);
                    swiping_code.setVisibility(View.INVISIBLE);

                } else if (payment.equals("Cash")) {
                    Row_Amount.setVisibility(View.VISIBLE);
                    Row_Bank.setVisibility(View.GONE);
                    Row_ChequeNumber.setVisibility(View.GONE);
                    Row_ChequeDate.setVisibility(View.GONE);
                    swiping_code.setVisibility(View.INVISIBLE);

                } else if (payment.equals("Swiping Machine")) {
                    Row_Amount.setVisibility(View.VISIBLE);
                    Row_Bank.setVisibility(View.GONE);
                    swiping_code.setVisibility(View.VISIBLE);

                    Row_ChequeNumber.setVisibility(View.GONE);
                    Row_ChequeDate.setVisibility(View.GONE);
                } else if (payment.equals("Select")) {
                    Row_Amount.setVisibility(View.GONE);
                    Row_Bank.setVisibility(View.GONE);
                    Row_ChequeNumber.setVisibility(View.GONE);
                    Row_ChequeDate.setVisibility(View.GONE);
                }
            }
        });


        Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                S_Payment = Paymentspinner.getText().toString();
                S_ChequeDate = ChequeDate.getText().toString();
                S_ChequeNumber = ChequeNumber.getText().toString();
                PaymentType = Paymentspinner.getText().toString();
                swpingcodes = enter_swiping_code.getText().toString();
                S_Amount = DepositAmountPaid.getText().toString();

                d = Double.parseDouble(S_Amount);



//                newAmount  = Double.parseDouble(S_Amount);


                filledcan = Filledcans.getText().toString();
                int a = Integer.parseInt(filledcan);

                if (paymentType.getText().toString().equals("COD")) {
                    if (S_Payment.equals("Select")) {
                        toaster.ShowErrorToast("Please Select Payment !");
                    }
                    else if (S_Payment.equals("Cash")) {

                        S_Bank = "";
                        S_ChequeDate = null;
                        S_ChequeNumber = "";
                        swiping_codes = "";


//                        if (S_Amount.equals("0")) {
//                            toaster.ShowErrorToast("Please Enter the Deposit Amount !");
//                        }
//                        else if (e < d) {
//                            toaster.ShowErrorToast("Please Enter Valid Amount !");
//                        }
//                        else if (e > d) {
//                            toaster.ShowErrorToast("Please Enter Valid Amount !");
//                        }
                         if (filledcan.equals("")) {
                            toaster.ShowErrorToast("Kindly Enter Filled Cans !");

                        } else if (a < requestacan) {
                            toaster.ShowErrorToast("Kindly Enter Filled Cans !");
                        } else if (a > requestacan) {
                            toaster.ShowErrorToast("Kindly Enter Filled Cans !");
                        } else if (!canCover.isChecked()) {
                            toaster.ShowErrorToast("Please Check the Washable Outer Cover !");
                        } else if (!pump.isChecked()) {
                            toaster.ShowErrorToast("Please Check the Automatic Dispensing Pump !");
                        } else if (!canhandle.isChecked()) {
                            toaster.ShowErrorToast("Please Check the Can Handle !");
                        } else if (!canopener.isChecked()) {
                            toaster.ShowErrorToast("Please Check the Cap Opener !");
                        } else {
//                            Toast.makeText(Dealer_Installation_Update_Activity.this, "Successfully", Toast.LENGTH_LONG).show();
                            newUpdateInstallation();
                        }
                    } else if (S_Payment.equals("Swiping Machine")) {
                        S_Bank = "";
                        S_ChequeDate = null;
                        S_ChequeNumber = "";
                        swiping_codes = "";

//                        if (S_Amount.equals("0")) {
//                            toaster.ShowErrorToast("Please Enter the Deposit Amount !");
//                        }
                        if (swpingcodes.equals("")) {
                            toaster.ShowErrorToast("Please Enter Approval Code !");
                        }
//                        else if (e < d) {
//                            toaster.ShowErrorToast("Please Enter Valid Amount !");
//                        } else if (e > d) {
//                            toaster.ShowErrorToast("Please Enter Valid Amount !");
//                        }
                        else if (filledcan.equals("")) {
                            toaster.ShowErrorToast("Kindly Enter Filled Cans !");

                        } else if (a < requestacan) {
                            toaster.ShowErrorToast("Kindly Enter Filled Cans !");
                        } else if (a > requestacan) {
                            toaster.ShowErrorToast("Kindly Enter Filled Cans !");
                        } else if (!canCover.isChecked()) {
                            toaster.ShowErrorToast("Please Check the Washable Outer Cover !");
                        } else if (!pump.isChecked()) {
                            toaster.ShowErrorToast("Please Check the Automatic Dispensing Pump !");
//                    Toast.makeText(Dealer_Installation_Update_Activity.this, "Successfully", Toast.LENGTH_LONG).show();
                        } else if (!canhandle.isChecked()) {
                            toaster.ShowErrorToast("Please Check the Can Handle !");
//                    Toast.makeText(Dealer_Installation_Update_Activity.this, "Successfully", Toast.LENGTH_LONG).show();
                        } else if (!canopener.isChecked()) {
                            toaster.ShowErrorToast("Please Check the Cap Opener !");
//                    Toast.makeText(Dealer_Installation_Update_Activity.this, "Successfully", Toast.LENGTH_LONG).show();
                        } else {
//                            Toast.makeText(Dealer_Installation_Update_Activity.this, "Successfully", Toast.LENGTH_SHORT).show();
                            newUpdateInstallation();
                        }
                    } else if (S_Payment.equals("Cheque")) {
                        if (S_Bank.equals("Select")) {
                            toaster.ShowErrorToast("Please Select Bank Name !");
                        } else {
                            if (S_ChequeNumber.equals("")) {
                                toaster.ShowErrorToast("Please Enter Cheque Number !");
                            } else if (S_ChequeNumber.length() < 6) {
                                toaster.ShowErrorToast("Please Enter Valid Cheque Number !");
                            } else if (S_ChequeDate.equals("")) {
                                toaster.ShowErrorToast("Please Select Cheque Date !");
                            }
//                            else if (S_Amount.equals("0")) {
//                                toaster.ShowErrorToast("Please Enter the Deposit Amount !");
//                            }
//                            else if (e < d) {
//                                toaster.ShowErrorToast("Please Enter Valid Amount !");
//                            } else if (e > d) {
//                                toaster.ShowErrorToast("Please Enter Valid Amount !");
//                            }
                            else if (filledcan.equals("")) {
                                toaster.ShowErrorToast("Kindly Enter Filled Cans !");

                            } else if (a < requestacan) {
                                toaster.ShowErrorToast("Kindly Enter Filled Cans !");
                            } else if (a > requestacan) {
                                toaster.ShowErrorToast("Kindly Enter Filled Cans !");
                            } else if (!canCover.isChecked()) {
                                toaster.ShowErrorToast("Please Check the Washable Outer Cover !");
                            } else if (!pump.isChecked()) {
                                toaster.ShowErrorToast("Please Check the Automatic Dispensing Pump !");
//                    Toast.makeText(Dealer_Installation_Update_Activity.this, "Successfully", Toast.LENGTH_LONG).show();
                            } else if (!canhandle.isChecked()) {
                                toaster.ShowErrorToast("Please Check the Can Handle !");
//                    Toast.makeText(Dealer_Installation_Update_Activity.this, "Successfully", Toast.LENGTH_LONG).show();
                            } else if (!canopener.isChecked()) {
                                toaster.ShowErrorToast("Please Check the Cap Opener !");
//                    Toast.makeText(Dealer_Installation_Update_Activity.this, "Successfully", Toast.LENGTH_LONG).show();
                            } else {
//                                Toast.makeText(Dealer_Installation_Update_Activity.this, "SuccessFully", Toast.LENGTH_LONG).show();
                                newUpdateInstallation();
                            }
                        }
                    } else {
//                        Toast.makeText(Dealer_Installation_Update_Activity.this, "Successfully", Toast.LENGTH_LONG).show();
                        newUpdateInstallation();
                    }
                } else if (paymentType.getText().toString().equals("Online")) {
//                    DepositAmountPaid.setText((int) TotalAmount);
//                    paymentType.setText("Online");
                    S_Amount = String.valueOf(TotalAmount + "");
                    S_Bank = "";
                    S_ChequeNumber = "";
                    S_ChequeDate = null;
                    if (filledcan.equals("")) {
                        toaster.ShowErrorToast("Kindly Enter Filled Cans !");

                    } else if (a < requestacan) {
                        toaster.ShowErrorToast("Kindly Enter Filled Cans !");
                    } else if (a > requestacan) {
                        toaster.ShowErrorToast("Kindly Enter Filled Cans !");
                    } else if (!canCover.isChecked()) {
                        toaster.ShowErrorToast("Please Check the Washable Outer Cover !");
                    } else if (!pump.isChecked()) {
                        toaster.ShowErrorToast("Please Check the Automatic Dispensing Pump !");
//                    Toast.makeText(Dealer_Installation_Update_Activity.this, "Successfully", Toast.LENGTH_LONG).show();
                    } else if (!canhandle.isChecked()) {
                        toaster.ShowErrorToast("Please Check the Can Handle !");
//                    Toast.makeText(Dealer_Installation_Update_Activity.this, "Successfully", Toast.LENGTH_LONG).show();
                    } else if (!canopener.isChecked()) {
                        toaster.ShowErrorToast("Please Check the Cap Opener !");
//                    Toast.makeText(Dealer_Installation_Update_Activity.this, "Successfully", Toast.LENGTH_LONG).show();
                    } else {
                        newUpdateInstallation();
                    }

                }

            }
        });

    }

    private void newUpdateInstallation() {
        if (network.CheckInternet()) {
            try {
                final ProgressDialog progressDialog = new ProgressDialog(Dealer_Installation_Update_Activity.this);
                progressDialog.setMessage("Loading...");
                progressDialog.show();
                APIService service = RetroClient.getApiService();


                Call<InstallationUpdate> call = service.NEWInstallationUpdate(
                        (int) Id,
                        Filledcans.getText().toString(),

                        CanCost,
                        TotalAmount,
                        Deposit,
                        myshare.getString(Shared.K_Code, ""),
                        paymentType.getText().toString(),
                        S_Payment,

                    //    S_Amount
                        e,

                        S_Bank,
                        S_ChequeNumber,
                        S_ChequeDate,
                        canCover.isChecked(),
                        pump.isChecked(),
                        CusCode,
                        canhandle.isChecked(),
                        canopener.isChecked(),
                        swpingcodes
//                        S_Bank

                );

                call.enqueue(new Callback<InstallationUpdate>() {
                    @Override
                    public void onResponse(Call<InstallationUpdate> call, Response<InstallationUpdate> response) {
                        progressDialog.dismiss();
                        switch (response.body().getResult()) {
                            case "Success": {
                                AlertDialogue alertnew = new AlertDialogue(Dealer_Installation_Update_Activity.this);
                                alertnew.showAlertbox("Customer Installation updated Successfully");
                                alertnew.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        finish();
                                    }
                                });
                                //finish();
                                break;
                            }
                            case "Already Updated": {
                                alert.showAlertbox("Installation already updated! Please check");
                                finish();
                                break;
                            }
                            case "Failure": {
                                alert.showAlertbox("Installation failed! Please try again");
                                finish();
                                break;

                            }
                            case "error": {
                                alert.showAlertbox("Server error in Installation! Please try again!");
                                finish();
                                break;

                            }
                            case "": {
                                alert.showAlertbox("Server error in Installation! Please Try Again!");
                                finish();
                                break;
                            }


                            default: {
                                alert.showAlertbox(getString(R.string.connection_slow));
                                finish();
                                //Horizontalscrollview.setVisibility(View.GONE);
                                //Body_layout.setVisibility(View.GONE);
                                break;
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<InstallationUpdate> call, Throwable t) {
                        progressDialog.dismiss();
//                        alert.showAlertbox(getString(R.string.connection_slow));
                        finish();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
                Log.v("UpdateException", e.getMessage());
            }
        } else {
            alert.showAlertbox(getString(R.string.connection_slow));
            finish();
        }
    }


    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Dealer_Welcome_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }

    private void getbankname() {
        final ProgressDialog progressDialog = new ProgressDialog(Dealer_Installation_Update_Activity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service = RetroClient.getApiService();
        Call<BankNames> call = service.banknames();
        call.enqueue(new Callback<BankNames>() {
            @Override
            public void onResponse(Call<BankNames> call, Response<BankNames> response) {
                try {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        banklist = response.body().getData();
                        bankspinner.setAdapter(new ArrayAdapter<String>(Dealer_Installation_Update_Activity.this, R.layout.support_simple_spinner_dropdown_item, banklist));
                        S_Bank = bankspinner.getSelectedItem().toString();
                    } else {
                        alert.showAlertbox("No Banks Avaliable");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
//                    box.showAlertboxnegative("Exception error please try again later");
                }
            }

            @Override
            public void onFailure(Call<BankNames> call, Throwable t) {
                progressDialog.dismiss();
                alert.showAlertbox("Failed to reach server. Please try again later");
            }
        });
    }


    public void Log_Out(View view) {
        new logout(this).log_out();
    }

    public void Popup_Menu(View view) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    String BarcodeValue = data.getStringExtra("barcode");
                    int position = data.getIntExtra("position", 0);
                  /*  Barcode barcode = data.getParcelableExtra("barcode");
                    int position = data.getIntExtra("position",0);*/
                    //Toast.makeText(this, "Barcode : "+barcode.displayValue+"\n"+position, Toast.LENGTH_LONG).show();
                    Log.v("Barcode Value", BarcodeValue);
                    Log.v("Customer Code", Code);
                    if (BarcodeValue.equals(Code)) {

                        toaster.ShowSuccessToast("QRCode successfully scanned");
                        QRCode.setText("Yes");//Barcodevale.setText("Barcode : "+barcode.displayValue );
                    } else {
                        toaster.ShowErrorToast("QRCode does not match Customer data");
                    }
                } else {
                    Toast.makeText(this, "Scanning failed! Please scan again.", Toast.LENGTH_LONG).show();

                    //  Barcodevale.setText("No value Found" );
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    public void Get_Coordinates(View view) {
        GPSTracker gps = new GPSTracker(Dealer_Installation_Update_Activity.this);

        // check if GPS enabled
        if (gps.canGetLocation()) {

            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
            Latitude.setText(String.format("%s", latitude));
            Longitude.setText(String.format("%s", longitude));

        } else {
            gps.showSettingsAlert();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void Scan_QRCode(View view) {

        GetPermission();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void GetPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    Snackbar.make(findViewById(R.id.dealer_installation_update), "You need give permission", Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void onClick(View v) {
                            //your action here
                            requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST);
                        }
                    }).show();

                } else {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST);
                }
            } else {
                ScanQRcode();
            }
        } else {
            ScanQRcode();
        }

    }

    private void ScanQRcode() {

        Intent a = new Intent(this, Scanning_Activity.class).putExtra("position", 0);
        startActivityForResult(a, 0);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            Snackbar.make(findViewById(R.id.dealer_installation_update), "Permission Granted",
                    Snackbar.LENGTH_LONG).show();
            ScanQRcode();

        } else {

            Snackbar.make(findViewById(R.id.dealer_installation_update), "Permission denied",
                    Snackbar.LENGTH_LONG).show();

        }
    }


    public void Cancel(View view) {
        onBackPressed();
    }
}


/*  LayoutInflater inflater = getLayoutInflater();
    View layout = inflater.inflate(R.layout.custom_toast,
            (ViewGroup) findViewById(R.id.custom_toast_container));
    TextView text = (TextView) layout.findViewById(R.id.text);
            text.setText("Press "+"'Back'"+" once again to exit!");
                    Toasts toast = new Toasts(getApplicationContext());
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL | Gravity.FILL_HORIZONTAL, 0, 0);
                    toast.setDuration(Toasts.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                     /*  StyleableToast st =
                    new StyleableToast(getApplicationContext(), "Cart is Empty !", Toasts.LENGTH_SHORT);
            st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
            st.setTextColor(Color.WHITE);
            st.setMaxAlpha();
            st.show();*/
