package com.brainmagic.toyumwater;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;

public class WebView_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        WebView webview = (WebView) findViewById(R.id.webview);

        webview.getSettings().setJavaScriptEnabled(true);
        String pdf = getIntent().getStringExtra("pdfurl");
        String urlpdf = "http://toyum.brainmagicllc.com/Pdf/" + pdf +".pdf";

        Log.v("Pdf",urlpdf);

        //String pdf2 = "http://www.adobe.com/devnet/acrobat/pdfs/pdf_open_paramters.pdf";
        webview.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + urlpdf);
        // webview.loadUrl("https://docs.google.com/gview?embedded=true&url="+ urlpdf);
        finish();
    }
}
