package com.brainmagic.toyumwater;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import alertbox.AlertDialogue;
import apiservice.APIService;
import butterknife.BindView;
import butterknife.ButterKnife;
import logout.logout;
import model.createcustomer.CreateCustomer;
import model.referral.ReferralResult;
import network.NetworkConnection;
import retroclient.RetroClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sharedpreference.Shared;
import toaster.Toasts;

public class Referall_Activity extends AppCompatActivity {
    @BindView(R.id.header_layout)
    LinearLayout Headerlayout;
    @BindView(R.id.cus_name)
    EditText Name;
    @BindView(R.id.cus_mobile)
    EditText Mobile;
    AlertDialogue box = new AlertDialogue(this);
    NetworkConnection network = new NetworkConnection(this);
    Toasts Toaster = new Toasts(this);
    private SharedPreferences myshare;
    String S_Name, S_Mobile, S_ReferNmae = "", S_ReferMobile = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referall);
        ButterKnife.bind(this);
        ImageView Logout = Headerlayout.findViewById(R.id.logout_image);
        TextView header_title = Headerlayout.findViewById(R.id.header_title);
        header_title.setText("Referral");
        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        if (myshare.getBoolean(Shared.K_Login, false))
            Logout.setVisibility(View.VISIBLE);
        else
            Logout.setVisibility(View.INVISIBLE);


        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        S_ReferNmae = myshare.getString(Shared.K_RegisterName, "");
        S_ReferMobile = myshare.getString(Shared.K_RegisterMobile, "");
    }

    public void Refeer_Customer(View view) {
        S_Name = Name.getText().toString();
        S_Mobile = Mobile.getText().toString();

        if (S_Name.isEmpty()) {
            Name.setFocusable(true);
            Name.setError("Please Enter Name");
        } else if (S_Mobile.isEmpty()) {
            Mobile.setFocusable(true);
            Mobile.setError("Please Enter Mobile number");
        } else if (S_Mobile.length() < 10) {
            Mobile.setFocusable(true);
            Mobile.setError("Please Enter valid Mobile number");
        }
        else if (network.CheckInternet()) {
            Create_Cuatomer();
        } else {
            box.showAlertbox(getString(R.string.no_network));
        }
    }

    private void Create_Cuatomer() {


        try {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();

            APIService service = RetroClient.getApiService();

            // Calling JSON

            Call<ReferralResult> call = service.REFERRAL_RESULT_CALL(
                    S_Name,
                    S_Mobile,
                    S_ReferNmae,
                    S_ReferMobile);
            call.enqueue(new Callback<ReferralResult>() {
                @Override
                public void onResponse(Call<ReferralResult> call, Response<ReferralResult> response) {
                    progressDialog.dismiss();

                    switch (response.body().getResult()) {
                        case "Success":

                            box.showAlertbox("Thank you for Referring a Customer!");
                            box.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    finish();
                                }
                            });
                            break;

                        case "Customer":
                            box.showAlertbox("Sorry! Referred person is already a Customer");
                            break;
                        case "Referral":
                            box.showAlertbox("Sorry! Already refereed this Customer");
                            break;
                        case "error":
                            box.showAlertbox("Server error in Update !");
                            break;

                        default:
                            box.showAlertbox(getString(R.string.connection_slow));
                            break;

                    }

                }

                @Override
                public void onFailure(Call<ReferralResult> call, Throwable t) {
                    progressDialog.dismiss();

                    box.showAlertbox(getString(R.string.connection_slow));
                }

            });
        } catch (Exception ex) {
            ex.getMessage();
            Log.v("Error", ex.getMessage());
        }
    }
    public void Log_Out(View view) {
        new logout(this).log_out();
    }
    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }

    public void Popup_Menu(View view) {
     /*   PopupMenu popupMenu = new PopupMenu(Login_Activity.this, view);
        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu pop) {
                // TODO Auto-generated method stub
                pop.dismiss();
            }
        });
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub
                switch (item.getItemId()) {
                    case R.id.about:
                        Intent about = new Intent(getApplicationContext(), Aboutus_Activity.class);
                        startActivity(about);
                        return true;
                    case R.id.products:
                        Intent services = new Intent(getApplicationContext(), Product_Activity.class);
                        startActivity(services);
                        return true;
                    case R.id.packaging:
                        Intent products = new Intent(getApplicationContext(),Packaging_Activity.class);
                        startActivity(products);
                        return true;
                    case R.id.login:
                        Intent login = new Intent(getApplicationContext(), Login_Activity.class);
                        startActivity(login);
                        return true;
                    case R.id.quality:
                        Intent quality = new Intent(getApplicationContext(), QualityControl_Activity.class);
                        startActivity(quality);
                        return true;
                    case R.id.contact:
                        Intent contact = new Intent(getApplicationContext(), Contact_Activity.class);
                        startActivity(contact);
                        return true;
                    case R.id.help:
                        Intent help = new Intent(getApplicationContext(), Helpdesk_Activity.class);
                        startActivity(help);
                        return true;
                    case R.id.documents:
                        *//*Intent documents = new Intent(getApplicationContext(), .class);
                        startActivity(documents);
                        return true;*//*


                }
                return false;
            }
        });
        popupMenu.inflate(R.menu.popupmenu);
        popupMenu.getMenu().findItem(R.id.login).setVisible(false);
        popupMenu.show();*/
    }
}
