package com.brainmagic.toyumwater;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.Manifest;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;



import java.io.IOException;
import java.util.List;

import info.androidhive.barcode.BarcodeReader;


public class Scanning_Activity extends AppCompatActivity  implements BarcodeReader.BarcodeReaderListener  {
  /*  private static final int PERMISSION_REQUEST = 100;
    SurfaceView cameraPreview;*/
    int pos;
    BarcodeReader barcodeReader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanning);

        pos = getIntent().getIntExtra("position",0);


/*
        cameraPreview = (SurfaceView) findViewById(R.id.camera_preview);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            GetPermission();
        }else
            createCamerasource();*/
        // get the barcode reader instance
        barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode_scanner);
    }

    @Override
    public void onScanned(Barcode barcode) {
        // playing barcode reader beep sound
        barcodeReader.playBeep();

        //Toast.makeText(this, barcode.displayValue, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent();
        intent.putExtra("barcode",barcode.displayValue);
        //barcodes.
        intent.putExtra("position",pos);
        setResult(CommonStatusCodes.SUCCESS,intent);
        finish();
        // ticket details activity by passing barcode
        /*Intent intent = new Intent(ScanActivity.this, TicketActivity.class);
        intent.putExtra("code", barcode.displayValue);
        startActivity(intent);*/
    }

    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String errorMessage) {

        Toast.makeText(getApplicationContext(), "Error occurred while scanning " + errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCameraPermissionDenied() {

    }





    /* @RequiresApi(api = Build.VERSION_CODES.M)
    private void GetPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission_group.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    Snackbar.make(findViewById(R.id.activity_scanning), "You need give permission", Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void onClick(View v) {
                            //your action here
                            requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST);
                        }
                    }).show();

                } else {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST);
                }
            } else {
                createCamerasource();
            }
        } else

        {
            createCamerasource();
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            Snackbar.make(findViewById(R.id.activity_scanning), "Permission Granted",
                    Snackbar.LENGTH_LONG).show();
            createCamerasource();

        } else {

            Snackbar.make(findViewById(R.id.activity_scanning), "Permission denied",
                    Snackbar.LENGTH_LONG).show();

        }
    }
    private void createCamerasource() {
        try {
            BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(this).build();
            final CameraSource cameraSource = new CameraSource.Builder(this, barcodeDetector).setAutoFocusEnabled(true).setRequestedPreviewSize(1600, 1024).build();
            cameraPreview.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {
                    if (ActivityCompat.checkSelfPermission(Scanning_Activity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        // ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        // public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        // int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    try {
                        cameraSource.start(cameraPreview.getHolder());
                    } catch (IOException e) {
                        //e.printStackTrace();
                        Log.v("Error Start Camera",e.getMessage());
                    }
                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    cameraSource.stop();
                }
            });
            barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
                @Override
                public void release() {
                }

                @Override
                public void receiveDetections(Detector.Detections<Barcode> detections) {
                    final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                    if(barcodes.size() > 0) {
                        Intent intent = new Intent();
                        intent.putExtra("barcode",barcodes.valueAt(0));
                        //barcodes.
                        intent.putExtra("position",pos);
                        setResult(CommonStatusCodes.SUCCESS,intent);
                        finish();
                    }

                }
            });
        } catch (Exception e) {
            Log.v("Error Create Camera",e.getMessage());

        }
    }
*/
}
