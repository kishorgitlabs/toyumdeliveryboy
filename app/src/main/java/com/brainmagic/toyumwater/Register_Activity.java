package com.brainmagic.toyumwater;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.iid.FirebaseInstanceId;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;

import apiservice.APIService;
import alertbox.Alert;
import model.Registration.RegisterResult;
import retroclient.RetroClient;
import sharedpreference.Shared;
import toaster.Toasts;
import fcm.Config;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class Register_Activity extends AppCompatActivity {


    private MaterialSpinner Usertype;
    private ArrayList<String> usertypeList;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private String token = "";
    Alert box = new Alert(Register_Activity.this);
    NetworkConnection network = new NetworkConnection(Register_Activity.this);
    Toasts Toaster = new Toasts(Register_Activity.this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    String S_Name,S_Usertype,S_Mobile,S_Email;
    private EditText Name,Mobile,Email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Usertype = (MaterialSpinner) findViewById(R.id.usertype);
        Name = (EditText) findViewById(R.id.name);
        Mobile = (EditText) findViewById(R.id.mobile);
        Email = (EditText) findViewById(R.id.email);

        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();

        usertypeList = new ArrayList<String>();
        usertypeList.add("Select Usertype");
        usertypeList.add("Customer");
        usertypeList.add("Dealer");
        usertypeList.add("Telecaller");
        usertypeList.add("TOYUM employee");
        usertypeList.add("Others");

        Usertype.setBackgroundResource(R.drawable.autotextback);
        Usertype.setItems(usertypeList);
        Usertype.setPadding(30, 0, 0, 0);
        Usertype.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                S_Usertype = item.toString();
                Log.v("Usertype", S_Usertype);

            }
        });
        //FCM
       /* mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    //Toast.makeText(getApplicationContext(), "Push Notification: " + message, Toast.LENGTH_LONG).show();

                }
            }
        };*/
         //displayFirebaseRegId();




    }
    // Fetches reg id from shared preferences
// and displays on the screen
    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        token = pref.getString("regId", null);
       // Toast.makeText(Register_Activity.this, token, Toast.LENGTH_LONG).show();
        // box.showAlertbox(token);
        Log.e(TAG, "Firebase reg id: " + token);

    }

    public void Register_User(View view) {
        S_Name = Name.getText().toString();
        S_Usertype = Usertype.getText().toString();
        S_Mobile = Mobile.getText().toString();
        S_Email = Email.getText().toString();

        if (S_Name.isEmpty()) {
            Name.setFocusable(true);
            Name.setError("Please Enter Name");
        } else if (S_Mobile.isEmpty()) {
            Mobile.setFocusable(true);
            Mobile.setError("Please Enter Mobile number");
        } else if (S_Mobile.length() < 10) {
            Mobile.setFocusable(true);
            Mobile.setError("Please Enter valid Mobile number");
        } else if (S_Usertype.equals("Select Usertype"))
            Toaster.ShowErrorToast("Please select Usertype");
        else if (network.CheckInternet()) {
            GenerateFirebaseId();
            //User_Registeration();
            //Toast.makeText(this, "Sucess", Toast.LENGTH_SHORT).show();
        } else {
            box.showAlertbox(getString(R.string.no_network));
        }

    }

    private void GenerateFirebaseId() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            token = FirebaseInstanceId.getInstance().getToken();
            Log.e(TAG, "Register token: " + token);

            progressDialog.dismiss();
            if (token != null && !token.equals("")) {
                User_Registeration();
            }else
            {
                Toaster.ShowErrorToast("Error in generating Instance ID !\nPlease try again !");
            }

        }
        catch (Exception ex)
        {
            Log.e(TAG,"Error GenerateFirebaseId : "+ ex.getMessage());
        }


    }

    private void User_Registeration() {


            try {
                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Registering...");
                progressDialog.show();

                APIService service = RetroClient.getApiService();

                // Calling JSON

                Call<RegisterResult> call = service.REGISTER_RESULT_CALL(S_Name,S_Email,S_Mobile,S_Usertype,token);
                call.enqueue(new Callback<RegisterResult>() {
                    @Override
                    public void onResponse(Call<RegisterResult> call, Response<RegisterResult> response) {
                        progressDialog.dismiss();

                        switch (response.body().getResult()) {
                            case "Success":
                                editor.putBoolean(Shared.K_Register, true);
                                editor.putString(Shared.K_RegisterName, S_Name);
                                editor.putString(Shared.K_RegisterMobile, S_Mobile);
                                editor.putString(Shared.K_RegisterEmail, S_Email);
                                editor.putString(Shared.K_RegisterType, S_Usertype);
                                editor.putLong(Shared.K_RegisterID, response.body().getData().getId());
                                editor.commit();
                                finish();
                                startActivity(new Intent(getApplicationContext(), Home_Activity.class));
                                break;

                            case "Failure":
                                box.showAlertbox("Error in Registration ! Please try again !");
                                break;

                            default:
                                box.showAlertbox(getString(R.string.connection_slow));
                                break;

                        }

                    }

                    @Override
                    public void onFailure(Call<RegisterResult> call, Throwable t) {
                        progressDialog.dismiss();

                        box.showAlertbox(getString(R.string.connection_slow));
                    }

                });
            } catch (Exception ex) {
                ex.getMessage();
                Log.v("Error", ex.getMessage());
            }
        }
}
