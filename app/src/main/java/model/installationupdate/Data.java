
package model.installationupdate;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("Amount")
    private String mAmount;
    @SerializedName("BankName")
    private String mBankName;
    @SerializedName("CanAmount")
    private String mCanAmount;
    @SerializedName("CanCover")
    private Boolean mCanCover;
    @SerializedName("Cans")
    private String mCans;
    @SerializedName("ChequeDate")
    private String mChequeDate;
    @SerializedName("ChequeNumber")
    private String mChequeNumber;
    @SerializedName("DealerCode")
    private String mDealerCode;
    @SerializedName("DepositPaid")
    private Object mDepositPaid;
    @SerializedName("DepositPayType")
    private Object mDepositPayType;
    @SerializedName("Id")
    private Long mId;
    @SerializedName("Latitude")
    private Double mLatitude;
    @SerializedName("Longitude")
    private Double mLongitude;
    @SerializedName("Mobile")
    private Object mMobile;
    @SerializedName("Pump")
    private Boolean mPump;
    @SerializedName("QRcode")
    private Object mQRcode;
    @SerializedName("TotalAmount")
    private String mTotalAmount;

    public String getAmount() {
        return mAmount;
    }

    public void setAmount(String amount) {
        mAmount = amount;
    }

    public String getBankName() {
        return mBankName;
    }

    public void setBankName(String bankName) {
        mBankName = bankName;
    }

    public String getCanAmount() {
        return mCanAmount;
    }

    public void setCanAmount(String canAmount) {
        mCanAmount = canAmount;
    }

    public Boolean getCanCover() {
        return mCanCover;
    }

    public void setCanCover(Boolean canCover) {
        mCanCover = canCover;
    }

    public String getCans() {
        return mCans;
    }

    public void setCans(String cans) {
        mCans = cans;
    }

    public String getChequeDate() {
        return mChequeDate;
    }

    public void setChequeDate(String chequeDate) {
        mChequeDate = chequeDate;
    }

    public String getChequeNumber() {
        return mChequeNumber;
    }

    public void setChequeNumber(String chequeNumber) {
        mChequeNumber = chequeNumber;
    }

    public String getDealerCode() {
        return mDealerCode;
    }

    public void setDealerCode(String dealerCode) {
        mDealerCode = dealerCode;
    }

    public Object getDepositPaid() {
        return mDepositPaid;
    }

    public void setDepositPaid(Object depositPaid) {
        mDepositPaid = depositPaid;
    }

    public Object getDepositPayType() {
        return mDepositPayType;
    }

    public void setDepositPayType(Object depositPayType) {
        mDepositPayType = depositPayType;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Double latitude) {
        mLatitude = latitude;
    }

    public Double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(Double longitude) {
        mLongitude = longitude;
    }

    public Object getMobile() {
        return mMobile;
    }

    public void setMobile(Object mobile) {
        mMobile = mobile;
    }

    public Boolean getPump() {
        return mPump;
    }

    public void setPump(Boolean pump) {
        mPump = pump;
    }

    public Object getQRcode() {
        return mQRcode;
    }

    public void setQRcode(Object qRcode) {
        mQRcode = qRcode;
    }

    public String getTotalAmount() {
        return mTotalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        mTotalAmount = totalAmount;
    }

}
