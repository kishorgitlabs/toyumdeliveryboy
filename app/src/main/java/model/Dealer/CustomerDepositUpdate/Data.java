
package model.Dealer.CustomerDepositUpdate;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("Amount")
    private Long mAmount;
    @SerializedName("BankName")
    private String mBankName;
    @SerializedName("ChequeDate")
    private Object mChequeDate;
    @SerializedName("ChequeNumber")
    private String mChequeNumber;
    @SerializedName("CollectedBy")
    private String mCollectedBy;
    @SerializedName("CreatedDate")
    private Object mCreatedDate;
    @SerializedName("CustomerCode")
    private String mCustomerCode;
    @SerializedName("CustomerName")
    private String mCustomerName;
    @SerializedName("Id")
    private Long mId;
    @SerializedName("PaymentFor")
    private String mPaymentFor;
    @SerializedName("PaymentType")
    private String mPaymentType;

    public Long getAmount() {
        return mAmount;
    }

    public void setAmount(Long Amount) {
        mAmount = Amount;
    }

    public String getBankName() {
        return mBankName;
    }

    public void setBankName(String BankName) {
        mBankName = BankName;
    }

    public Object getChequeDate() {
        return mChequeDate;
    }

    public void setChequeDate(Object ChequeDate) {
        mChequeDate = ChequeDate;
    }

    public String getChequeNumber() {
        return mChequeNumber;
    }

    public void setChequeNumber(String ChequeNumber) {
        mChequeNumber = ChequeNumber;
    }

    public String getCollectedBy() {
        return mCollectedBy;
    }

    public void setCollectedBy(String CollectedBy) {
        mCollectedBy = CollectedBy;
    }

    public Object getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(Object CreatedDate) {
        mCreatedDate = CreatedDate;
    }

    public String getCustomerCode() {
        return mCustomerCode;
    }

    public void setCustomerCode(String CustomerCode) {
        mCustomerCode = CustomerCode;
    }

    public String getCustomerName() {
        return mCustomerName;
    }

    public void setCustomerName(String CustomerName) {
        mCustomerName = CustomerName;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long Id) {
        mId = Id;
    }

    public String getPaymentFor() {
        return mPaymentFor;
    }

    public void setPaymentFor(String PaymentFor) {
        mPaymentFor = PaymentFor;
    }

    public String getPaymentType() {
        return mPaymentType;
    }

    public void setPaymentType(String PaymentType) {
        mPaymentType = PaymentType;
    }

}
