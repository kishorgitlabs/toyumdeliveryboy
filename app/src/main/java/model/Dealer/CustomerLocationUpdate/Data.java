
package model.Dealer.CustomerLocationUpdate;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("Address")
    private Object mAddress;
    @SerializedName("Area")
    private Object mArea;
    @SerializedName("BankName")
    private Object mBankName;
    @SerializedName("CansLimit")
    private Object mCansLimit;
    @SerializedName("Center")
    private Object mCenter;
    @SerializedName("ChequeDate")
    private Object mChequeDate;
    @SerializedName("ChequeNumber")
    private Object mChequeNumber;
    @SerializedName("City")
    private Object mCity;
    @SerializedName("Comments")
    private Object mComments;
    @SerializedName("CreatedDate")
    private Object mCreatedDate;
    @SerializedName("CustomerCode")
    private String mCustomerCode;
    @SerializedName("CustomerName")
    private Object mCustomerName;
    @SerializedName("CustomerStatus")
    private Object mCustomerStatus;
    @SerializedName("CustomerType")
    private Object mCustomerType;
    @SerializedName("CustomerZone")
    private Object mCustomerZone;
    @SerializedName("DealerCode")
    private Object mDealerCode;
    @SerializedName("DealerMobile")
    private Object mDealerMobile;
    @SerializedName("DealerName")
    private Object mDealerName;
    @SerializedName("DepositAmount")
    private Object mDepositAmount;
    @SerializedName("DepositAmountPaid")
    private Object mDepositAmountPaid;
    @SerializedName("DepositPaid")
    private Object mDepositPaid;
    @SerializedName("DepositPaidDate")
    private Object mDepositPaidDate;
    @SerializedName("DepositPayType")
    private Object mDepositPayType;
    @SerializedName("DoorNo")
    private Object mDoorNo;
    @SerializedName("Email")
    private Object mEmail;
    @SerializedName("Id")
    private Long mId;
    @SerializedName("InstallationBy")
    private Object mInstallationBy;
    @SerializedName("InstalledBy")
    private Object mInstalledBy;
    @SerializedName("InstalledDate")
    private Object mInstalledDate;
    @SerializedName("Landmark")
    private Object mLandmark;
    @SerializedName("Langitude")
    private Double mLangitude;
    @SerializedName("Latitude")
    private Double mLatitude;
    @SerializedName("Mobile")
    private Object mMobile;
    @SerializedName("Password")
    private Object mPassword;
    @SerializedName("PayType")
    private Object mPayType;
    @SerializedName("Pincode")
    private Object mPincode;
    @SerializedName("QRCode")
    private Object mQRCode;
    @SerializedName("ReasonForInactive")
    private Object mReasonForInactive;
    @SerializedName("SalesExecutive")
    private Object mSalesExecutive;
    @SerializedName("SplRate")
    private Object mSplRate;
    @SerializedName("State")
    private Object mState;
    @SerializedName("Street")
    private Object mStreet;
    @SerializedName("UpdatedDate")
    private Object mUpdatedDate;
    @SerializedName("UserName")
    private Object mUserName;
    @SerializedName("UserType")
    private Object mUserType;

    public Object getAddress() {
        return mAddress;
    }

    public void setAddress(Object Address) {
        mAddress = Address;
    }

    public Object getArea() {
        return mArea;
    }

    public void setArea(Object Area) {
        mArea = Area;
    }

    public Object getBankName() {
        return mBankName;
    }

    public void setBankName(Object BankName) {
        mBankName = BankName;
    }

    public Object getCansLimit() {
        return mCansLimit;
    }

    public void setCansLimit(Object CansLimit) {
        mCansLimit = CansLimit;
    }

    public Object getCenter() {
        return mCenter;
    }

    public void setCenter(Object Center) {
        mCenter = Center;
    }

    public Object getChequeDate() {
        return mChequeDate;
    }

    public void setChequeDate(Object ChequeDate) {
        mChequeDate = ChequeDate;
    }

    public Object getChequeNumber() {
        return mChequeNumber;
    }

    public void setChequeNumber(Object ChequeNumber) {
        mChequeNumber = ChequeNumber;
    }

    public Object getCity() {
        return mCity;
    }

    public void setCity(Object City) {
        mCity = City;
    }

    public Object getComments() {
        return mComments;
    }

    public void setComments(Object Comments) {
        mComments = Comments;
    }

    public Object getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(Object CreatedDate) {
        mCreatedDate = CreatedDate;
    }

    public String getCustomerCode() {
        return mCustomerCode;
    }

    public void setCustomerCode(String CustomerCode) {
        mCustomerCode = CustomerCode;
    }

    public Object getCustomerName() {
        return mCustomerName;
    }

    public void setCustomerName(Object CustomerName) {
        mCustomerName = CustomerName;
    }

    public Object getCustomerStatus() {
        return mCustomerStatus;
    }

    public void setCustomerStatus(Object CustomerStatus) {
        mCustomerStatus = CustomerStatus;
    }

    public Object getCustomerType() {
        return mCustomerType;
    }

    public void setCustomerType(Object CustomerType) {
        mCustomerType = CustomerType;
    }

    public Object getCustomerZone() {
        return mCustomerZone;
    }

    public void setCustomerZone(Object CustomerZone) {
        mCustomerZone = CustomerZone;
    }

    public Object getDealerCode() {
        return mDealerCode;
    }

    public void setDealerCode(Object DealerCode) {
        mDealerCode = DealerCode;
    }

    public Object getDealerMobile() {
        return mDealerMobile;
    }

    public void setDealerMobile(Object DealerMobile) {
        mDealerMobile = DealerMobile;
    }

    public Object getDealerName() {
        return mDealerName;
    }

    public void setDealerName(Object DealerName) {
        mDealerName = DealerName;
    }

    public Object getDepositAmount() {
        return mDepositAmount;
    }

    public void setDepositAmount(Object DepositAmount) {
        mDepositAmount = DepositAmount;
    }

    public Object getDepositAmountPaid() {
        return mDepositAmountPaid;
    }

    public void setDepositAmountPaid(Object DepositAmountPaid) {
        mDepositAmountPaid = DepositAmountPaid;
    }

    public Object getDepositPaid() {
        return mDepositPaid;
    }

    public void setDepositPaid(Object DepositPaid) {
        mDepositPaid = DepositPaid;
    }

    public Object getDepositPaidDate() {
        return mDepositPaidDate;
    }

    public void setDepositPaidDate(Object DepositPaidDate) {
        mDepositPaidDate = DepositPaidDate;
    }

    public Object getDepositPayType() {
        return mDepositPayType;
    }

    public void setDepositPayType(Object DepositPayType) {
        mDepositPayType = DepositPayType;
    }

    public Object getDoorNo() {
        return mDoorNo;
    }

    public void setDoorNo(Object DoorNo) {
        mDoorNo = DoorNo;
    }

    public Object getEmail() {
        return mEmail;
    }

    public void setEmail(Object Email) {
        mEmail = Email;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long Id) {
        mId = Id;
    }

    public Object getInstallationBy() {
        return mInstallationBy;
    }

    public void setInstallationBy(Object InstallationBy) {
        mInstallationBy = InstallationBy;
    }

    public Object getInstalledBy() {
        return mInstalledBy;
    }

    public void setInstalledBy(Object InstalledBy) {
        mInstalledBy = InstalledBy;
    }

    public Object getInstalledDate() {
        return mInstalledDate;
    }

    public void setInstalledDate(Object InstalledDate) {
        mInstalledDate = InstalledDate;
    }

    public Object getLandmark() {
        return mLandmark;
    }

    public void setLandmark(Object Landmark) {
        mLandmark = Landmark;
    }

    public Double getLangitude() {
        return mLangitude;
    }

    public void setLangitude(Double Langitude) {
        mLangitude = Langitude;
    }

    public Double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Double Latitude) {
        mLatitude = Latitude;
    }

    public Object getMobile() {
        return mMobile;
    }

    public void setMobile(Object Mobile) {
        mMobile = Mobile;
    }

    public Object getPassword() {
        return mPassword;
    }

    public void setPassword(Object Password) {
        mPassword = Password;
    }

    public Object getPayType() {
        return mPayType;
    }

    public void setPayType(Object PayType) {
        mPayType = PayType;
    }

    public Object getPincode() {
        return mPincode;
    }

    public void setPincode(Object Pincode) {
        mPincode = Pincode;
    }

    public Object getQRCode() {
        return mQRCode;
    }

    public void setQRCode(Object QRCode) {
        mQRCode = QRCode;
    }

    public Object getReasonForInactive() {
        return mReasonForInactive;
    }

    public void setReasonForInactive(Object ReasonForInactive) {
        mReasonForInactive = ReasonForInactive;
    }

    public Object getSalesExecutive() {
        return mSalesExecutive;
    }

    public void setSalesExecutive(Object SalesExecutive) {
        mSalesExecutive = SalesExecutive;
    }

    public Object getSplRate() {
        return mSplRate;
    }

    public void setSplRate(Object SplRate) {
        mSplRate = SplRate;
    }

    public Object getState() {
        return mState;
    }

    public void setState(Object State) {
        mState = State;
    }

    public Object getStreet() {
        return mStreet;
    }

    public void setStreet(Object Street) {
        mStreet = Street;
    }

    public Object getUpdatedDate() {
        return mUpdatedDate;
    }

    public void setUpdatedDate(Object UpdatedDate) {
        mUpdatedDate = UpdatedDate;
    }

    public Object getUserName() {
        return mUserName;
    }

    public void setUserName(Object UserName) {
        mUserName = UserName;
    }

    public Object getUserType() {
        return mUserType;
    }

    public void setUserType(Object UserType) {
        mUserType = UserType;
    }

}
