
package model.Dealer.demopendinginstalls;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Datum {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("Area")
    private String mArea;
    @SerializedName("BankName")
    private Object mBankName;
    @SerializedName("CansLimit")
    private Long mCansLimit;
    @SerializedName("Center")
    private String mCenter;
    @SerializedName("ChequeDate")
    private Object mChequeDate;
    @SerializedName("ChequeNumber")
    private Object mChequeNumber;
    @SerializedName("City")
    private String mCity;
    @SerializedName("Comments")
    private Object mComments;
    @SerializedName("CreatedDate")
    private String mCreatedDate;
    @SerializedName("CustomerCode")
    private String mCustomerCode;
    @SerializedName("CustomerName")
    private String mCustomerName;
    @SerializedName("CustomerStatus")
    private String mCustomerStatus;
    @SerializedName("CustomerType")
    private String mCustomerType;
    @SerializedName("CustomerZone")
    private String mCustomerZone;
    @SerializedName("DealerCode")
    private String mDealerCode;
    @SerializedName("DealerName")
    private String mDealerName;
    @SerializedName("DepositAmount")
    private Long mDepositAmount;
    @SerializedName("DepositAmountPaid")
    private Object mDepositAmountPaid;
    @SerializedName("DepositPaid")
    private Object mDepositPaid;
    @SerializedName("DepositPaidDate")
    private Object mDepositPaidDate;
    @SerializedName("DepositPayType")
    private Object mDepositPayType;
    @SerializedName("DoorNo")
    private String mDoorNo;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("Email2")
    private String mEmail2;
    @SerializedName("flag")
    private String mFlag;
    @SerializedName("FlatName")
    private String mFlatName;
    @SerializedName("FlatNo")
    private String mFlatNo;
    @SerializedName("Floor")
    private String mFloor;
    @SerializedName("Id")
    private Long mId;
    @SerializedName("InstallationBy")
    private String mInstallationBy;
    @SerializedName("InstalledBy")
    private String mInstalledBy;
    @SerializedName("InstalledDate")
    private String mInstalledDate;
    @SerializedName("Landmark")
    private String mLandmark;
    @SerializedName("Langitude")
    private Object mLangitude;
    @SerializedName("Latitude")
    private Object mLatitude;
    @SerializedName("Mobile")
    private String mMobile;
    @SerializedName("Mobile2")
    private String mMobile2;
    @SerializedName("Password")
    private String mPassword;
    @SerializedName("PayType")
    private String mPayType;
    @SerializedName("Pincode")
    private String mPincode;
    @SerializedName("QRCode")
    private Object mQRCode;
    @SerializedName("ReasonForInactive")
    private Object mReasonForInactive;
    @SerializedName("ReferEmail")
    private String mReferEmail;
    @SerializedName("ReferMobile")
    private String mReferMobile;
    @SerializedName("ReferName")
    private String mReferName;
    @SerializedName("ReferUsertype")
    private String mReferUsertype;
    @SerializedName("regId")
    private Object mRegId;
    @SerializedName("SalesExecutive")
    private Object mSalesExecutive;
    @SerializedName("SplRate")
    private Object mSplRate;
    @SerializedName("State")
    private String mState;
    @SerializedName("Street")
    private String mStreet;
    @SerializedName("UpdatedDate")
    private Object mUpdatedDate;
    @SerializedName("UserName")
    private String mUserName;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String Address) {
        mAddress = Address;
    }

    public String getArea() {
        return mArea;
    }

    public void setArea(String Area) {
        mArea = Area;
    }

    public Object getBankName() {
        return mBankName;
    }

    public void setBankName(Object BankName) {
        mBankName = BankName;
    }

    public Long getCansLimit() {
        return mCansLimit;
    }

    public void setCansLimit(Long CansLimit) {
        mCansLimit = CansLimit;
    }

    public String getCenter() {
        return mCenter;
    }

    public void setCenter(String Center) {
        mCenter = Center;
    }

    public Object getChequeDate() {
        return mChequeDate;
    }

    public void setChequeDate(Object ChequeDate) {
        mChequeDate = ChequeDate;
    }

    public Object getChequeNumber() {
        return mChequeNumber;
    }

    public void setChequeNumber(Object ChequeNumber) {
        mChequeNumber = ChequeNumber;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String City) {
        mCity = City;
    }

    public Object getComments() {
        return mComments;
    }

    public void setComments(Object Comments) {
        mComments = Comments;
    }

    public String getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(String CreatedDate) {
        mCreatedDate = CreatedDate;
    }

    public String getCustomerCode() {
        return mCustomerCode;
    }

    public void setCustomerCode(String CustomerCode) {
        mCustomerCode = CustomerCode;
    }

    public String getCustomerName() {
        return mCustomerName;
    }

    public void setCustomerName(String CustomerName) {
        mCustomerName = CustomerName;
    }

    public String getCustomerStatus() {
        return mCustomerStatus;
    }

    public void setCustomerStatus(String CustomerStatus) {
        mCustomerStatus = CustomerStatus;
    }

    public String getCustomerType() {
        return mCustomerType;
    }

    public void setCustomerType(String CustomerType) {
        mCustomerType = CustomerType;
    }

    public String getCustomerZone() {
        return mCustomerZone;
    }

    public void setCustomerZone(String CustomerZone) {
        mCustomerZone = CustomerZone;
    }

    public String getDealerCode() {
        return mDealerCode;
    }

    public void setDealerCode(String DealerCode) {
        mDealerCode = DealerCode;
    }

    public String getDealerName() {
        return mDealerName;
    }

    public void setDealerName(String DealerName) {
        mDealerName = DealerName;
    }

    public Long getDepositAmount() {
        return mDepositAmount;
    }

    public void setDepositAmount(Long DepositAmount) {
        mDepositAmount = DepositAmount;
    }

    public Object getDepositAmountPaid() {
        return mDepositAmountPaid;
    }

    public void setDepositAmountPaid(Object DepositAmountPaid) {
        mDepositAmountPaid = DepositAmountPaid;
    }

    public Object getDepositPaid() {
        return mDepositPaid;
    }

    public void setDepositPaid(Object DepositPaid) {
        mDepositPaid = DepositPaid;
    }

    public Object getDepositPaidDate() {
        return mDepositPaidDate;
    }

    public void setDepositPaidDate(Object DepositPaidDate) {
        mDepositPaidDate = DepositPaidDate;
    }

    public Object getDepositPayType() {
        return mDepositPayType;
    }

    public void setDepositPayType(Object DepositPayType) {
        mDepositPayType = DepositPayType;
    }

    public String getDoorNo() {
        return mDoorNo;
    }

    public void setDoorNo(String DoorNo) {
        mDoorNo = DoorNo;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String Email) {
        mEmail = Email;
    }

    public String getEmail2() {
        return mEmail2;
    }

    public void setEmail2(String Email2) {
        mEmail2 = Email2;
    }

    public String getFlag() {
        return mFlag;
    }

    public void setFlag(String flag) {
        mFlag = flag;
    }

    public String getFlatName() {
        return mFlatName;
    }

    public void setFlatName(String FlatName) {
        mFlatName = FlatName;
    }

    public String getFlatNo() {
        return mFlatNo;
    }

    public void setFlatNo(String FlatNo) {
        mFlatNo = FlatNo;
    }

    public String getFloor() {
        return mFloor;
    }

    public void setFloor(String Floor) {
        mFloor = Floor;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long Id) {
        mId = Id;
    }

    public String getInstallationBy() {
        return mInstallationBy;
    }

    public void setInstallationBy(String InstallationBy) {
        mInstallationBy = InstallationBy;
    }

    public String getInstalledBy() {
        return mInstalledBy;
    }

    public void setInstalledBy(String InstalledBy) {
        mInstalledBy = InstalledBy;
    }

    public String getInstalledDate() {
        return mInstalledDate;
    }

    public void setInstalledDate(String InstalledDate) {
        mInstalledDate = InstalledDate;
    }

    public String getLandmark() {
        return mLandmark;
    }

    public void setLandmark(String Landmark) {
        mLandmark = Landmark;
    }

    public Object getLangitude() {
        return mLangitude;
    }

    public void setLangitude(Object Langitude) {
        mLangitude = Langitude;
    }

    public Object getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Object Latitude) {
        mLatitude = Latitude;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String Mobile) {
        mMobile = Mobile;
    }

    public String getMobile2() {
        return mMobile2;
    }

    public void setMobile2(String Mobile2) {
        mMobile2 = Mobile2;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String Password) {
        mPassword = Password;
    }

    public String getPayType() {
        return mPayType;
    }

    public void setPayType(String PayType) {
        mPayType = PayType;
    }

    public String getPincode() {
        return mPincode;
    }

    public void setPincode(String Pincode) {
        mPincode = Pincode;
    }

    public Object getQRCode() {
        return mQRCode;
    }

    public void setQRCode(Object QRCode) {
        mQRCode = QRCode;
    }

    public Object getReasonForInactive() {
        return mReasonForInactive;
    }

    public void setReasonForInactive(Object ReasonForInactive) {
        mReasonForInactive = ReasonForInactive;
    }

    public String getReferEmail() {
        return mReferEmail;
    }

    public void setReferEmail(String ReferEmail) {
        mReferEmail = ReferEmail;
    }

    public String getReferMobile() {
        return mReferMobile;
    }

    public void setReferMobile(String ReferMobile) {
        mReferMobile = ReferMobile;
    }

    public String getReferName() {
        return mReferName;
    }

    public void setReferName(String ReferName) {
        mReferName = ReferName;
    }

    public String getReferUsertype() {
        return mReferUsertype;
    }

    public void setReferUsertype(String ReferUsertype) {
        mReferUsertype = ReferUsertype;
    }

    public Object getRegId() {
        return mRegId;
    }

    public void setRegId(Object regId) {
        mRegId = regId;
    }

    public Object getSalesExecutive() {
        return mSalesExecutive;
    }

    public void setSalesExecutive(Object SalesExecutive) {
        mSalesExecutive = SalesExecutive;
    }

    public Object getSplRate() {
        return mSplRate;
    }

    public void setSplRate(Object SplRate) {
        mSplRate = SplRate;
    }

    public String getState() {
        return mState;
    }

    public void setState(String State) {
        mState = State;
    }

    public String getStreet() {
        return mStreet;
    }

    public void setStreet(String Street) {
        mStreet = Street;
    }

    public Object getUpdatedDate() {
        return mUpdatedDate;
    }

    public void setUpdatedDate(Object UpdatedDate) {
        mUpdatedDate = UpdatedDate;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String UserName) {
        mUserName = UserName;
    }

}
