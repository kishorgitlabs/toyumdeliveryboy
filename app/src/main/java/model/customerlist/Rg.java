
package model.customerlist;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Rg {

    @SerializedName("appid")
    private String mAppid;
    @SerializedName("createddate")
    private String mCreateddate;
    @SerializedName("data")
    private Data mData;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("Id")
    private Long mId;
    @SerializedName("mobile")
    private String mMobile;
    @SerializedName("name")
    private Object mName;
    @SerializedName("OtpVerification")
    private String mOtpVerification;
    @SerializedName("updateddate")
    private Object mUpdateddate;
    @SerializedName("usertype")
    private String mUsertype;

    public String getAppid() {
        return mAppid;
    }

    public void setAppid(String appid) {
        mAppid = appid;
    }

    public String getCreateddate() {
        return mCreateddate;
    }

    public void setCreateddate(String createddate) {
        mCreateddate = createddate;
    }

    public Data getData() {
        return mData;
    }

    public void setData(Data data) {
        mData = data;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String mobile) {
        mMobile = mobile;
    }

    public Object getName() {
        return mName;
    }

    public void setName(Object name) {
        mName = name;
    }

    public String getOtpVerification() {
        return mOtpVerification;
    }

    public void setOtpVerification(String otpVerification) {
        mOtpVerification = otpVerification;
    }

    public Object getUpdateddate() {
        return mUpdateddate;
    }

    public void setUpdateddate(Object updateddate) {
        mUpdateddate = updateddate;
    }

    public String getUsertype() {
        return mUsertype;
    }

    public void setUsertype(String usertype) {
        mUsertype = usertype;
    }

}
