
package model.Customer.EditCustomerInfo;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("Area")
    private Object mArea;
    @SerializedName("CansLimit")
    private Object mCansLimit;
    @SerializedName("Center")
    private Object mCenter;
    @SerializedName("City")
    private String mCity;
    @SerializedName("Comments")
    private Object mComments;
    @SerializedName("CreatedDate")
    private Object mCreatedDate;
    @SerializedName("CustomerCode")
    private String mCustomerCode;
    @SerializedName("CustomerName")
    private String mCustomerName;
    @SerializedName("CustomerStatus")
    private Object mCustomerStatus;
    @SerializedName("CustomerType")
    private Object mCustomerType;
    @SerializedName("CustomerZone")
    private Object mCustomerZone;
    @SerializedName("DealerCode")
    private Object mDealerCode;
    @SerializedName("DealerMobile")
    private Object mDealerMobile;
    @SerializedName("DealerName")
    private Object mDealerName;
    @SerializedName("DepositAmount")
    private Object mDepositAmount;
    @SerializedName("DepositAmountPaid")
    private Object mDepositAmountPaid;
    @SerializedName("DepositPaidDate")
    private Object mDepositPaidDate;
    @SerializedName("DoorNo")
    private String mDoorNo;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("Id")
    private Long mId;
    @SerializedName("InstalledBy")
    private Object mInstalledBy;
    @SerializedName("InstalledDate")
    private Object mInstalledDate;
    @SerializedName("Landmark")
    private String mLandmark;
    @SerializedName("Langitude")
    private Object mLangitude;
    @SerializedName("Latitude")
    private Object mLatitude;
    @SerializedName("Mobile")
    private String mMobile;
    @SerializedName("Password")
    private Object mPassword;
    @SerializedName("PayType")
    private Object mPayType;
    @SerializedName("Pincode")
    private Object mPincode;
    @SerializedName("QRCode")
    private Object mQRCode;
    @SerializedName("ReasonForInactive")
    private Object mReasonForInactive;
    @SerializedName("SalesExecutive")
    private Object mSalesExecutive;
    @SerializedName("SplRate")
    private Object mSplRate;
    @SerializedName("State")
    private String mState;
    @SerializedName("Street")
    private String mStreet;
    @SerializedName("UpdatedDate")
    private Object mUpdatedDate;
    @SerializedName("UserName")
    private Object mUserName;
    @SerializedName("UserType")
    private Object mUserType;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String Address) {
        mAddress = Address;
    }

    public Object getArea() {
        return mArea;
    }

    public void setArea(Object Area) {
        mArea = Area;
    }

    public Object getCansLimit() {
        return mCansLimit;
    }

    public void setCansLimit(Object CansLimit) {
        mCansLimit = CansLimit;
    }

    public Object getCenter() {
        return mCenter;
    }

    public void setCenter(Object Center) {
        mCenter = Center;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String City) {
        mCity = City;
    }

    public Object getComments() {
        return mComments;
    }

    public void setComments(Object Comments) {
        mComments = Comments;
    }

    public Object getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(Object CreatedDate) {
        mCreatedDate = CreatedDate;
    }

    public String getCustomerCode() {
        return mCustomerCode;
    }

    public void setCustomerCode(String CustomerCode) {
        mCustomerCode = CustomerCode;
    }

    public String getCustomerName() {
        return mCustomerName;
    }

    public void setCustomerName(String CustomerName) {
        mCustomerName = CustomerName;
    }

    public Object getCustomerStatus() {
        return mCustomerStatus;
    }

    public void setCustomerStatus(Object CustomerStatus) {
        mCustomerStatus = CustomerStatus;
    }

    public Object getCustomerType() {
        return mCustomerType;
    }

    public void setCustomerType(Object CustomerType) {
        mCustomerType = CustomerType;
    }

    public Object getCustomerZone() {
        return mCustomerZone;
    }

    public void setCustomerZone(Object CustomerZone) {
        mCustomerZone = CustomerZone;
    }

    public Object getDealerCode() {
        return mDealerCode;
    }

    public void setDealerCode(Object DealerCode) {
        mDealerCode = DealerCode;
    }

    public Object getDealerMobile() {
        return mDealerMobile;
    }

    public void setDealerMobile(Object DealerMobile) {
        mDealerMobile = DealerMobile;
    }

    public Object getDealerName() {
        return mDealerName;
    }

    public void setDealerName(Object DealerName) {
        mDealerName = DealerName;
    }

    public Object getDepositAmount() {
        return mDepositAmount;
    }

    public void setDepositAmount(Object DepositAmount) {
        mDepositAmount = DepositAmount;
    }

    public Object getDepositAmountPaid() {
        return mDepositAmountPaid;
    }

    public void setDepositAmountPaid(Object DepositAmountPaid) {
        mDepositAmountPaid = DepositAmountPaid;
    }

    public Object getDepositPaidDate() {
        return mDepositPaidDate;
    }

    public void setDepositPaidDate(Object DepositPaidDate) {
        mDepositPaidDate = DepositPaidDate;
    }

    public String getDoorNo() {
        return mDoorNo;
    }

    public void setDoorNo(String DoorNo) {
        mDoorNo = DoorNo;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String Email) {
        mEmail = Email;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long Id) {
        mId = Id;
    }

    public Object getInstalledBy() {
        return mInstalledBy;
    }

    public void setInstalledBy(Object InstalledBy) {
        mInstalledBy = InstalledBy;
    }

    public Object getInstalledDate() {
        return mInstalledDate;
    }

    public void setInstalledDate(Object InstalledDate) {
        mInstalledDate = InstalledDate;
    }

    public String getLandmark() {
        return mLandmark;
    }

    public void setLandmark(String Landmark) {
        mLandmark = Landmark;
    }

    public Object getLangitude() {
        return mLangitude;
    }

    public void setLangitude(Object Langitude) {
        mLangitude = Langitude;
    }

    public Object getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Object Latitude) {
        mLatitude = Latitude;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String Mobile) {
        mMobile = Mobile;
    }

    public Object getPassword() {
        return mPassword;
    }

    public void setPassword(Object Password) {
        mPassword = Password;
    }

    public Object getPayType() {
        return mPayType;
    }

    public void setPayType(Object PayType) {
        mPayType = PayType;
    }

    public Object getPincode() {
        return mPincode;
    }

    public void setPincode(Object Pincode) {
        mPincode = Pincode;
    }

    public Object getQRCode() {
        return mQRCode;
    }

    public void setQRCode(Object QRCode) {
        mQRCode = QRCode;
    }

    public Object getReasonForInactive() {
        return mReasonForInactive;
    }

    public void setReasonForInactive(Object ReasonForInactive) {
        mReasonForInactive = ReasonForInactive;
    }

    public Object getSalesExecutive() {
        return mSalesExecutive;
    }

    public void setSalesExecutive(Object SalesExecutive) {
        mSalesExecutive = SalesExecutive;
    }

    public Object getSplRate() {
        return mSplRate;
    }

    public void setSplRate(Object SplRate) {
        mSplRate = SplRate;
    }

    public String getState() {
        return mState;
    }

    public void setState(String State) {
        mState = State;
    }

    public String getStreet() {
        return mStreet;
    }

    public void setStreet(String Street) {
        mStreet = Street;
    }

    public Object getUpdatedDate() {
        return mUpdatedDate;
    }

    public void setUpdatedDate(Object UpdatedDate) {
        mUpdatedDate = UpdatedDate;
    }

    public Object getUserName() {
        return mUserName;
    }

    public void setUserName(Object UserName) {
        mUserName = UserName;
    }

    public Object getUserType() {
        return mUserType;
    }

    public void setUserType(Object UserType) {
        mUserType = UserType;
    }

}
