
package model.Customer.CustomerInfo;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("Area")
    private String mArea;
    @SerializedName("BankName")
    private Object mBankName;
    @SerializedName("CansLimit")
    private Long mCansLimit;
    @SerializedName("Center")
    private String mCenter;
    @SerializedName("ChequeDate")
    private Object mChequeDate;
    @SerializedName("ChequeNumber")
    private Object mChequeNumber;
    @SerializedName("City")
    private String mCity;
    @SerializedName("Comments")
    private Object mComments;
    @SerializedName("CreatedDate")
    private String mCreatedDate;
    @SerializedName("CustomerCode")
    private String mCustomerCode;
    @SerializedName("CustomerName")
    private String mCustomerName;
    @SerializedName("CustomerStatus")
    private String mCustomerStatus;
    @SerializedName("CustomerType")
    private String mCustomerType;
    @SerializedName("CustomerZone")
    private String mCustomerZone;
    @SerializedName("DealerCode")
    private String mDealerCode;
    @SerializedName("DealerMobile")
    private String mDealerMobile;
    @SerializedName("DealerName")
    private String mDealerName;
    @SerializedName("DepositAmount")
    private Long mDepositAmount;
    @SerializedName("DepositAmountPaid")
    private Long mDepositAmountPaid;
    @SerializedName("DepositPaid")
    private String mDepositPaid;
    @SerializedName("DepositPaidDate")
    private String mDepositPaidDate;
    @SerializedName("DepositPayType")
    private String mDepositPayType;
    @SerializedName("DoorNo")
    private String mDoorNo;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("Email2")
    private Object mEmail2;
    @SerializedName("FlatName")
    private Object mFlatName;
    @SerializedName("FlatNo")
    private Object mFlatNo;
    @SerializedName("Floor")
    private Object mFloor;
    @SerializedName("Id")
    private Long mId;
    @SerializedName("InstallationBy")
    private String mInstallationBy;
    @SerializedName("InstalledBy")
    private String mInstalledBy;
    @SerializedName("InstalledDate")
    private String mInstalledDate;
    @SerializedName("Landmark")
    private String mLandmark;
    @SerializedName("Langitude")
    private Double mLangitude;
    @SerializedName("Latitude")
    private Double mLatitude;
    @SerializedName("Mobile")
    private String mMobile;
    @SerializedName("Mobile2")
    private Object mMobile2;
    @SerializedName("Password")
    private String mPassword;
    @SerializedName("PayType")
    private String mPayType;
    @SerializedName("Pincode")
    private String mPincode;
    @SerializedName("QRCode")
    private String mQRCode;
    @SerializedName("ReasonForInactive")
    private String mReasonForInactive;
    @SerializedName("regId")
    private Long mRegId;
    @SerializedName("SalesExecutive")
    private String mSalesExecutive;
    @SerializedName("SplRate")
    private Object mSplRate;
    @SerializedName("State")
    private String mState;
    @SerializedName("Street")
    private String mStreet;
    @SerializedName("UpdatedDate")
    private String mUpdatedDate;
    @SerializedName("UserName")
    private String mUserName;
    @SerializedName("UserType")
    private Object mUserType;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String Address) {
        mAddress = Address;
    }

    public String getArea() {
        return mArea;
    }

    public void setArea(String Area) {
        mArea = Area;
    }

    public Object getBankName() {
        return mBankName;
    }

    public void setBankName(Object BankName) {
        mBankName = BankName;
    }

    public Long getCansLimit() {
        return mCansLimit;
    }

    public void setCansLimit(Long CansLimit) {
        mCansLimit = CansLimit;
    }

    public String getCenter() {
        return mCenter;
    }

    public void setCenter(String Center) {
        mCenter = Center;
    }

    public Object getChequeDate() {
        return mChequeDate;
    }

    public void setChequeDate(Object ChequeDate) {
        mChequeDate = ChequeDate;
    }

    public Object getChequeNumber() {
        return mChequeNumber;
    }

    public void setChequeNumber(Object ChequeNumber) {
        mChequeNumber = ChequeNumber;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String City) {
        mCity = City;
    }

    public Object getComments() {
        return mComments;
    }

    public void setComments(Object Comments) {
        mComments = Comments;
    }

    public String getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(String CreatedDate) {
        mCreatedDate = CreatedDate;
    }

    public String getCustomerCode() {
        return mCustomerCode;
    }

    public void setCustomerCode(String CustomerCode) {
        mCustomerCode = CustomerCode;
    }

    public String getCustomerName() {
        return mCustomerName;
    }

    public void setCustomerName(String CustomerName) {
        mCustomerName = CustomerName;
    }

    public String getCustomerStatus() {
        return mCustomerStatus;
    }

    public void setCustomerStatus(String CustomerStatus) {
        mCustomerStatus = CustomerStatus;
    }

    public String getCustomerType() {
        return mCustomerType;
    }

    public void setCustomerType(String CustomerType) {
        mCustomerType = CustomerType;
    }

    public String getCustomerZone() {
        return mCustomerZone;
    }

    public void setCustomerZone(String CustomerZone) {
        mCustomerZone = CustomerZone;
    }

    public String getDealerCode() {
        return mDealerCode;
    }

    public void setDealerCode(String DealerCode) {
        mDealerCode = DealerCode;
    }

    public String getDealerMobile() {
        return mDealerMobile;
    }

    public void setDealerMobile(String DealerMobile) {
        mDealerMobile = DealerMobile;
    }

    public String getDealerName() {
        return mDealerName;
    }

    public void setDealerName(String DealerName) {
        mDealerName = DealerName;
    }

    public Long getDepositAmount() {
        return mDepositAmount;
    }

    public void setDepositAmount(Long DepositAmount) {
        mDepositAmount = DepositAmount;
    }

    public Long getDepositAmountPaid() {
        return mDepositAmountPaid;
    }

    public void setDepositAmountPaid(Long DepositAmountPaid) {
        mDepositAmountPaid = DepositAmountPaid;
    }

    public String getDepositPaid() {
        return mDepositPaid;
    }

    public void setDepositPaid(String DepositPaid) {
        mDepositPaid = DepositPaid;
    }

    public String getDepositPaidDate() {
        return mDepositPaidDate;
    }

    public void setDepositPaidDate(String DepositPaidDate) {
        mDepositPaidDate = DepositPaidDate;
    }

    public String getDepositPayType() {
        return mDepositPayType;
    }

    public void setDepositPayType(String DepositPayType) {
        mDepositPayType = DepositPayType;
    }

    public String getDoorNo() {
        return mDoorNo;
    }

    public void setDoorNo(String DoorNo) {
        mDoorNo = DoorNo;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String Email) {
        mEmail = Email;
    }

    public Object getEmail2() {
        return mEmail2;
    }

    public void setEmail2(Object Email2) {
        mEmail2 = Email2;
    }

    public Object getFlatName() {
        return mFlatName;
    }

    public void setFlatName(Object FlatName) {
        mFlatName = FlatName;
    }

    public Object getFlatNo() {
        return mFlatNo;
    }

    public void setFlatNo(Object FlatNo) {
        mFlatNo = FlatNo;
    }

    public Object getFloor() {
        return mFloor;
    }

    public void setFloor(Object Floor) {
        mFloor = Floor;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long Id) {
        mId = Id;
    }

    public String getInstallationBy() {
        return mInstallationBy;
    }

    public void setInstallationBy(String InstallationBy) {
        mInstallationBy = InstallationBy;
    }

    public String getInstalledBy() {
        return mInstalledBy;
    }

    public void setInstalledBy(String InstalledBy) {
        mInstalledBy = InstalledBy;
    }

    public String getInstalledDate() {
        return mInstalledDate;
    }

    public void setInstalledDate(String InstalledDate) {
        mInstalledDate = InstalledDate;
    }

    public String getLandmark() {
        return mLandmark;
    }

    public void setLandmark(String Landmark) {
        mLandmark = Landmark;
    }

    public Double getLangitude() {
        return mLangitude;
    }

    public void setLangitude(Double Langitude) {
        mLangitude = Langitude;
    }

    public Double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Double Latitude) {
        mLatitude = Latitude;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String Mobile) {
        mMobile = Mobile;
    }

    public Object getMobile2() {
        return mMobile2;
    }

    public void setMobile2(Object Mobile2) {
        mMobile2 = Mobile2;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String Password) {
        mPassword = Password;
    }

    public String getPayType() {
        return mPayType;
    }

    public void setPayType(String PayType) {
        mPayType = PayType;
    }

    public String getPincode() {
        return mPincode;
    }

    public void setPincode(String Pincode) {
        mPincode = Pincode;
    }

    public String getQRCode() {
        return mQRCode;
    }

    public void setQRCode(String QRCode) {
        mQRCode = QRCode;
    }

    public String getReasonForInactive() {
        return mReasonForInactive;
    }

    public void setReasonForInactive(String ReasonForInactive) {
        mReasonForInactive = ReasonForInactive;
    }

    public Long getRegId() {
        return mRegId;
    }

    public void setRegId(Long regId) {
        mRegId = regId;
    }

    public String getSalesExecutive() {
        return mSalesExecutive;
    }

    public void setSalesExecutive(String SalesExecutive) {
        mSalesExecutive = SalesExecutive;
    }

    public Object getSplRate() {
        return mSplRate;
    }

    public void setSplRate(Object SplRate) {
        mSplRate = SplRate;
    }

    public String getState() {
        return mState;
    }

    public void setState(String State) {
        mState = State;
    }

    public String getStreet() {
        return mStreet;
    }

    public void setStreet(String Street) {
        mStreet = Street;
    }

    public String getUpdatedDate() {
        return mUpdatedDate;
    }

    public void setUpdatedDate(String UpdatedDate) {
        mUpdatedDate = UpdatedDate;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String UserName) {
        mUserName = UserName;
    }

    public Object getUserType() {
        return mUserType;
    }

    public void setUserType(Object UserType) {
        mUserType = UserType;
    }

}
