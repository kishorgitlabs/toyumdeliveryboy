
package model.Customer.OrderHistory;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Datum {

    @SerializedName("CallDate")
    private String mCallDate;
    @SerializedName("CreatedDate")
    private String mCreatedDate;
    @SerializedName("CustomerAddress")
    private String mCustomerAddress;
    @SerializedName("CustomerCode")
    private String mCustomerCode;
    @SerializedName("CustomerMobile")
    private String mCustomerMobile;
    @SerializedName("CustomerName")
    private String mCustomerName;
    @SerializedName("DealerCode")
    private String mDealerCode;
    @SerializedName("DealerName")
    private String mDealerName;
    @SerializedName("DeliveryDate")
    private String mDeliveryDate;
    @SerializedName("EmptyCans")
    private Long mEmptyCans;
    @SerializedName("Id")
    private Long mId;
    @SerializedName("Latitude")
    private Double mLatitude;
    @SerializedName("Longitude")
    private Double mLongitude;
    @SerializedName("OrderNo")
    private String mOrderNo;
    @SerializedName("QRCode")
    private String mQRCode;
    @SerializedName("RefillCans")
    private Long mRefillCans;
    @SerializedName("RefillCansDeliverd")
    private Long mRefillCansDeliverd;
    @SerializedName("RefillStatus")
    private String mRefillStatus;
    @SerializedName("Remarks")
    private String mRemarks;
    @SerializedName("SuppliedDate")
    private String mSuppliedDate;
    @SerializedName("UpdatedDate")
    private String mUpdatedDate;

    public String getCallDate() {
        return mCallDate;
    }

    public void setCallDate(String CallDate) {
        mCallDate = CallDate;
    }

    public String getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(String CreatedDate) {
        mCreatedDate = CreatedDate;
    }

    public String getCustomerAddress() {
        return mCustomerAddress;
    }

    public void setCustomerAddress(String CustomerAddress) {
        mCustomerAddress = CustomerAddress;
    }

    public String getCustomerCode() {
        return mCustomerCode;
    }

    public void setCustomerCode(String CustomerCode) {
        mCustomerCode = CustomerCode;
    }

    public String getCustomerMobile() {
        return mCustomerMobile;
    }

    public void setCustomerMobile(String CustomerMobile) {
        mCustomerMobile = CustomerMobile;
    }

    public String getCustomerName() {
        return mCustomerName;
    }

    public void setCustomerName(String CustomerName) {
        mCustomerName = CustomerName;
    }

    public String getDealerCode() {
        return mDealerCode;
    }

    public void setDealerCode(String DealerCode) {
        mDealerCode = DealerCode;
    }

    public String getDealerName() {
        return mDealerName;
    }

    public void setDealerName(String DealerName) {
        mDealerName = DealerName;
    }

    public String getDeliveryDate() {
        return mDeliveryDate;
    }

    public void setDeliveryDate(String DeliveryDate) {
        mDeliveryDate = DeliveryDate;
    }

    public Long getEmptyCans() {
        return mEmptyCans;
    }

    public void setEmptyCans(Long EmptyCans) {
        mEmptyCans = EmptyCans;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long Id) {
        mId = Id;
    }

    public Double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Double Latitude) {
        mLatitude = Latitude;
    }

    public Double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(Double Longitude) {
        mLongitude = Longitude;
    }

    public String getOrderNo() {
        return mOrderNo;
    }

    public void setOrderNo(String OrderNo) {
        mOrderNo = OrderNo;
    }

    public String getQRCode() {
        return mQRCode;
    }

    public void setQRCode(String QRCode) {
        mQRCode = QRCode;
    }

    public Long getRefillCans() {
        return mRefillCans;
    }

    public void setRefillCans(Long RefillCans) {
        mRefillCans = RefillCans;
    }

    public Long getRefillCansDeliverd() {
        return mRefillCansDeliverd;
    }

    public void setRefillCansDeliverd(Long RefillCansDeliverd) {
        mRefillCansDeliverd = RefillCansDeliverd;
    }

    public String getRefillStatus() {
        return mRefillStatus;
    }

    public void setRefillStatus(String RefillStatus) {
        mRefillStatus = RefillStatus;
    }

    public String getRemarks() {
        return mRemarks;
    }

    public void setRemarks(String Remarks) {
        mRemarks = Remarks;
    }

    public String getSuppliedDate() {
        return mSuppliedDate;
    }

    public void setSuppliedDate(String SuppliedDate) {
        mSuppliedDate = SuppliedDate;
    }

    public String getUpdatedDate() {
        return mUpdatedDate;
    }

    public void setUpdatedDate(String UpdatedDate) {
        mUpdatedDate = UpdatedDate;
    }

}
