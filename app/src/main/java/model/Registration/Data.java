
package model.Registration;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("appid")
    private String mAppid;
    @SerializedName("createddate")
    private String mCreateddate;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("Id")
    private Long mId;
    @SerializedName("mobile")
    private String mMobile;
    @SerializedName("name")
    private String mName;
    @SerializedName("updateddate")
    private String mUpdateddate;
    @SerializedName("usertype")
    private String mUsertype;

    public String getAppid() {
        return mAppid;
    }

    public void setAppid(String appid) {
        mAppid = appid;
    }

    public String getCreateddate() {
        return mCreateddate;
    }

    public void setCreateddate(String createddate) {
        mCreateddate = createddate;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long Id) {
        mId = Id;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String mobile) {
        mMobile = mobile;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getUpdateddate() {
        return mUpdateddate;
    }

    public void setUpdateddate(String updateddate) {
        mUpdateddate = updateddate;
    }

    public String getUsertype() {
        return mUsertype;
    }

    public void setUsertype(String usertype) {
        mUsertype = usertype;
    }

}
