
package model.ChangePassword;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("Code")
    private String mCode;
    @SerializedName("NewPassword")
    private String mNewPassword;
    @SerializedName("OldPassword")
    private String mOldPassword;

    public String getCode() {
        return mCode;
    }

    public void setCode(String Code) {
        mCode = Code;
    }

    public String getNewPassword() {
        return mNewPassword;
    }

    public void setNewPassword(String NewPassword) {
        mNewPassword = NewPassword;
    }

    public String getOldPassword() {
        return mOldPassword;
    }

    public void setOldPassword(String OldPassword) {
        mOldPassword = OldPassword;
    }

}
