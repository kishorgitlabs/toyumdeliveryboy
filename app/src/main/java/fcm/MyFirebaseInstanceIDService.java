package fcm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import static fcm.Config.K_FIREBASEID;

/**
 * Created by system01 on 1/24/2017.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    SharedPreferences pref;
    Context context;
    SharedPreferences.Editor editor;

    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();


        @Override
        public void onTokenRefresh() {
            super.onTokenRefresh();
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            Log.e(TAG, "sendRegistrationToServer: " + refreshedToken);
            // Saving reg id to shared preferences
            storeRegIdInPref(refreshedToken);

            // sending reg id to your server
      //      sendRegistrationToServer(refreshedToken);

            // Notify UI that registration has completed, so the progress indicator can be hidden.
            Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
            registrationComplete.putExtra("token", refreshedToken);
            LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
        }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);

    }

    private void storeRegIdInPref(String token) {
        //= getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences pref;
        pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        editor = pref.edit();
        //editor.putString("regId", token);K_FIREBASEID
        editor.putString(K_FIREBASEID, token);
        editor.apply();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
