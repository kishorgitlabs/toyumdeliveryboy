package fcm;

import java.util.Random;

/**
 * Created by system01 on 1/24/2017.
 */

public class Config {
    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";
    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    // id to handle the notification in the notification tray
    public static final Random random = new Random();
    public static final int NOTIFICATION_ID = random.nextInt(9999 - 1000) + 1000;
    public static final int NOTIFICATION_ID_BIG_IMAGE = random.nextInt(9999 - 1000) + 1000;

    public static final String SHARED_PREF = "ah_firebase";
    public static final String K_FIREBASEID = "regId";
}
