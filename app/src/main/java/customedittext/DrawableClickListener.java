package customedittext;

/**
 * Created by SYSTEM02 on 2/9/2018.
 */

public interface DrawableClickListener {

    public static enum DrawablePosition { TOP, BOTTOM, LEFT, RIGHT };
    public void onClick(DrawablePosition target);
}