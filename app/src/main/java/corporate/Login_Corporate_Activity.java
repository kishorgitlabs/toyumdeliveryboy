package corporate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

public class Login_Corporate_Activity extends AppCompatActivity {
    RelativeLayout DC_layout,Billing_layout,Payments_layout;
    ImageView Back,Home;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_corporate);
        DC_layout = (RelativeLayout) findViewById(R.id.chalan_layout);
        Billing_layout = (RelativeLayout) findViewById(R.id.billing);
        Payments_layout = (RelativeLayout) findViewById(R.id.payments);
        Back = (ImageView)findViewById(R.id.back);
        Home = (ImageView)findViewById(R.id.home);

        DC_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(),Corporate_DC_Activity.class));
            }
        });

        Billing_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),Corporate_Billing_Activity.class));
            }
        });

        Payments_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),Corporate_Payment_Activity.class));
            }
        });
        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(getApplicationContext(), Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });
    }
}
