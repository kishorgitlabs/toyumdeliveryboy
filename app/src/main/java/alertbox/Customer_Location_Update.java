package alertbox;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.brainmagic.toyumwater.R;

import java.util.List;

import apiservice.APIService;
import model.Dealer.CustomerList.Datum;
import model.Dealer.CustomerLocationUpdate.CustomerLocationupdate;
import retroclient.RetroClient;
import sharedpreference.Shared;
import toaster.Toasts;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import service.GPSTracker;

import static android.content.Context.MODE_PRIVATE;


public class Customer_Location_Update {
	private Context context;
	private List<Datum> InstallationList;
	private TextView CustomerName, CustomerCode, Latitude,Longitude;

	private Button Cancel,Update,Get;
	int Position;
	double latitude; // latitude
	double longitude; // longitude

	long Id, Deposit, cans;
	String S_CustomerName, S_CustomerCode, S_Latitude,S_Longitude;
	Alert alert;
	NetworkConnection network;
	Toasts toaster;
	private SharedPreferences myshare;
	private SharedPreferences.Editor editor;
	private AlertDialog alertDialog;



	public Customer_Location_Update(Context con, List<Datum> data, int listPosition) {
		// TODO Auto-generated constructor stub
		this.context = con;
		this.alert = new Alert(context);
		this.network = new NetworkConnection(context);
		this.toaster = new Toasts(context);
		this.InstallationList = data;
		this.Position = listPosition;
			}


	public void showupdatebox()
	{

		myshare = context.getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);

		alertDialog = new Builder(context).create();
		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.location_update, null);
		alertDialog.setView(dialogView);


		CustomerName = (TextView) dialogView.findViewById(R.id.customername);
		CustomerCode = (TextView) dialogView.findViewById(R.id.customercode);
		Latitude = (TextView) dialogView.findViewById(R.id.latitude);
		Longitude = (TextView) dialogView.findViewById(R.id.longitude);

		Update = (Button) dialogView.findViewById(R.id.update);
		Get = (Button) dialogView.findViewById(R.id.get);
		Cancel = (Button) dialogView.findViewById(R.id.cancel);

		CustomerName.setText(InstallationList.get(Position).getCustomerName());
		CustomerCode.setText(InstallationList.get(Position).getCustomerCode());

		Id = InstallationList.get(Position).getId();
		Get.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				GPSTracker gps = new GPSTracker(context);

				// check if GPS enabled
				if(gps.canGetLocation()){

					latitude = gps.getLatitude();
					longitude = gps.getLongitude();

					Latitude.setText(String.format("%s",latitude));
					Longitude.setText(String.format("%s",longitude));

					// \n is for new line
					//Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
				}else{
					// can't get location
					// GPS or Network is not enabled
					// Ask user to enable GPS/network in settings
					gps.showSettingsAlert();
				}


				//getLocation();
			}
		});


		Update.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				 if (Latitude.getText().toString().equals("")) {
							toaster.ShowErrorToast("Kindly Get Coordinates !");
						} else if (Longitude.getText().toString().equals("")) {
					 		toaster.ShowErrorToast("Kindly Get Coordinates !");
				 } else if (network.CheckInternet()) {
							UpdateInstallation();
						} else {
							alert.showAlertbox(context.getString(R.string.no_network));
						}
			}
		});

		Cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				alertDialog.dismiss();

			}
		});

		alertDialog.show();
	}



	private void UpdateInstallation() {
		final ProgressDialog progressDialog = new ProgressDialog(context);
		progressDialog.setMessage("Loading...");
		progressDialog.show();
		APIService service = RetroClient.getApiService();

		Call<CustomerLocationupdate> call = service.CUSTOMER_LOCATIONUPDATE_CALL(InstallationList.get(Position).getCustomerCode(),Latitude.getText().toString(),Longitude.getText().toString());
		Log.v("Code",myshare.getString(Shared.K_Code,""));
		call.enqueue(new Callback<CustomerLocationupdate>() {
			@Override
			public void onResponse(Call<CustomerLocationupdate> call, Response<CustomerLocationupdate> response) {
				progressDialog.dismiss();
				switch (response.body().getResult()) {
					case "Success": {

						AlertDialogue alertnew = new AlertDialogue(context);
						alertnew.showAlertbox("Customer location updated Successfully");
						alertnew.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
							@Override
							public void onDismiss(DialogInterface dialog) {
								alertDialog.dismiss();
								((Activity) context).recreate();
							}
						});
						//finish();
						break;
					}
					case "Failure": {
						alertDialog.dismiss();
						alert.showAlertbox("Updated failed, Please try again !");

						((Activity) context).recreate();
						break;
					}
					default: {
						AlertDialogue alertnew = new AlertDialogue(context);
						alertnew.showAlertbox(context.getString(R.string.connection_slow));
						alertnew.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
							@Override
							public void onDismiss(DialogInterface dialog) {
								alertDialog.dismiss();
								((Activity) context).recreate();
							}
						});
						break;
					}
				}
			}

			@Override
			public void onFailure(Call<CustomerLocationupdate> call, Throwable t) {
				alertDialog.dismiss();
				progressDialog.dismiss();
				alert.showAlertbox(context.getString(R.string.connection_slow));

			}
		});
	}











}










