package alertbox;



import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;


/**
 * Created by Systems02 on 22-May-17.
 */

public class Alert {

    private  Context context;
    AlertDialog.Builder alertDialog;

    public Alert(Context context) {
        this.context = context;
    }

    public void showAlertbox(String msg) {
        alertDialog = new AlertDialog.Builder(
                context);
        alertDialog.setMessage(msg);
        alertDialog.setTitle("TOYUM");
        alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        //alertDialog.setIcon(R.drawable.logo);
        alertDialog.show();
        /*alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        })*/
    }


}
