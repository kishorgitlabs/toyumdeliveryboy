package alertbox;


public class EditProfile {
	/*private Context context;

	private EditText CustomerName, CustomerMobile,CustomerEmail,Landmark,DoorNo,Sreet,Address;
	private EditText DepositAmountPaid, Filledcans, Remarks;
	private Button Update,Cancel;
	private Customerinfo customerinfo;
	long Id,cans;
	String Name, Code, QRCode = "";
	Alert alert;
	NetworkConnection network;
	Toasts toaster;
	private SharedPreferences myshare;
	private SharedPreferences.Editor editor;
	private AlertDialog alertDialog;

	public EditProfile(Context con, Customerinfo customerinfo) {
		// TODO Auto-generated constructor stub
		this.context = con;
		this.alert = new Alert(context);
		this.network = new NetworkConnection(context);
		this.toaster = new Toasts(context);
		this.customerinfo = customerinfo;

		//this.Position = listPosition;
			}

 	//editor = myshare.edit();
	public void show_edit_profile()
	{

		myshare = context.getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);

		alertDialog = new Builder(context).create();
		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.edit_profile, null);
		alertDialog.setView(dialogView);
		//TextView log = (TextView) dialogView.findViewById(R.id.loglabel);


		CustomerName = (EditText) dialogView.findViewById(R.id.customername);
		CustomerMobile = (EditText) dialogView.findViewById(R.id.customermobile);
		CustomerEmail = (EditText) dialogView.findViewById(R.id.customeremail);
		DoorNo = (EditText) dialogView.findViewById(R.id.door);
		Sreet = (EditText) dialogView.findViewById(R.id.street);
		Address = (EditText) dialogView.findViewById(R.id.address);
		CustomerMobile = (EditText) dialogView.findViewById(R.id.customermobile);
		Landmark = (EditText) dialogView.findViewById(R.id.landmark);

		Update = (Button) dialogView.findViewById(R.id.update);
		Cancel = (Button) dialogView.findViewById(R.id.cancel);

		CustomerName.setText(customerinfo.getData().getCustomerName());
		CustomerMobile.setText(customerinfo.getData().getMobile());
		CustomerEmail.setText(customerinfo.getData().getEmail());
		DoorNo.setText(customerinfo.getData().getDoorNo());
		Sreet.setText(customerinfo.getData().getStreet());
		Address.setText(customerinfo.getData().getAddress());
		Landmark.setText(customerinfo.getData().getLandmark());

		Cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				alertDialog.dismiss();

			}
		});

		Update.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (network.CheckInternet()) {
					UpdateProfile();
				} else {
					alert.showAlertbox(context.getString(R.string.no_network));
				}


				*//*QRCode = spinner.getText().toString();
				if (DepositAmountPaid.getText().toString().equals("")) {
					toaster.ShowErrorToast("Kindly enter deposit amount !");
				} else if (Filledcans.getText().toString().equals("")) {
					toaster.ShowErrorToast("Kindly enter Filled Cans !");
				} else if (QRCode.equals("Select")) {
					toaster.ShowErrorToast("Kindly select QR code option !");
				} else if (network.CheckInternet()) {
					UpdateInstallation();
				} else {
					alert.showAlertbox(context.getString(R.string.no_network));
				}*//*

			}
		});

		alertDialog.show();
	}



	public void UpdateProfile() {
		final ProgressDialog progressDialog = new ProgressDialog(context);
		progressDialog.setMessage("Loading...");
		progressDialog.show();
		APIService service = RetroClient.getApiService();

		Call<EditCustomer> call = service.EDIT_CUSTOMER_CALL(myshare.getString(Shared.K_Code, ""),
				CustomerName.getText().toString(),
				DoorNo.getText().toString(),
				Sreet.getText().toString(),
				Address.getText().toString(),
				Landmark.getText().toString(),
				CustomerMobile.getText().toString(),
				CustomerEmail.getText().toString()
				);
		call.enqueue(new Callback<EditCustomer>() {
			@Override
			public void onResponse(Call<EditCustomer> call, Response<EditCustomer> response) {
				progressDialog.dismiss();
				switch (response.body().getResult()) {
					case "Success": {
						alertDialog.dismiss();
						AlertDialogue alertnew = new AlertDialogue(context);
						alertnew.showAlertbox("Profile updated successfully");
						alertnew.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
							@Override
							public void onDismiss(DialogInterface dialog) {
								((Activity) context).recreate();
							}
						});
						//finish();
						break;
					}
					case "Failure": {
						alertDialog.dismiss();
						alert.showAlertbox("Error in updating !, Please try again !");

						((Activity) context).recreate();
						break;
					}
					default: {
						alertDialog.dismiss();
						alert.showAlertbox(context.getString(R.string.connection_slow));
						//Horizontalscrollview.setVisibility(View.GONE);
						//Body_layout.setVisibility(View.GONE);

						((Activity) context).recreate();
						break;
					}
				}
			}

			@Override
			public void onFailure(Call<EditCustomer> call, Throwable t) {
				progressDialog.dismiss();
				alertDialog.dismiss();
				alert.showAlertbox(context.getString(R.string.connection_slow));
				//Horizontalscrollview.setVisibility(View.GONE);

			}
		});
	}



	void forgetpassword(String email){
		final ProgressDialog progressDialog = new ProgressDialog(context);
		progressDialog.setMessage("Loading...");
		progressDialog.show();

		APIService service = RetroClient.getApiService();
		Call<ForgetPassword> call = service.Forget_Password(email);
		call.enqueue(new Callback<ForgetPassword>() {
			@Override
			public void onResponse(Call<ForgetPassword> call, Response<ForgetPassword> response) {
				progressDialog.dismiss();
				switch (response.body().getResult())
				{
					case "Success":
						alert.showAlertbox("Password has been sent to your Email successfully");
						break;
					case "MailIdNotFound":
						alert.showAlertbox("Please enter the registered Email!");
						break;
					default: {
						alert.showAlertbox("Connection interrupted! Please try again");
						break;
					}
				}
			}

			@Override
			public void onFailure(Call<ForgetPassword> call, Throwable t)
			{
				progressDialog.dismiss();
				alert.showAlertbox("Connection interrupted! Please try again");


			}
		});
	}





*/


}










