package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.toyumwater.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import model.Dealer.DealerDeliveryHistory.Datum;

/**
 * Created by Systems02 on 05-Jul-17.
 */

public class DealerDeliveryHistoryadapter extends RecyclerView.Adapter<DealerDeliveryHistoryadapter.MyViewHolder> {

    private List<Datum> HistoryList;
    private Context context;
    private static final int FOOTER_VIEW = 1;
    int sum;
    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView SNo,Text1,Text2,Text3,Text4,Text5,Text6,Text7,Text8,Amount,Amount2,Amount3;
        public ImageView Update;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.SNo = (TextView) itemView.findViewById(R.id.sno);
            this.Text1 = (TextView) itemView.findViewById(R.id.text1);
            this.Text2 = (TextView) itemView.findViewById(R.id.text2);
            this.Text3 = (TextView) itemView.findViewById(R.id.text3);
            this.Text4 = (TextView) itemView.findViewById(R.id.text4);
            this.Text5 = (TextView) itemView.findViewById(R.id.text5);
            this.Text6 = (TextView) itemView.findViewById(R.id.text6);
            this.Text7 = (TextView) itemView.findViewById(R.id.text7);
            this.Text8 = (TextView) itemView.findViewById(R.id.text8);
            this.Amount = (TextView) itemView.findViewById(R.id.amount);
            this.Amount2 = (TextView) itemView.findViewById(R.id.amount2);
            this.Amount3 = (TextView) itemView.findViewById(R.id.amount3);
            //this.Update = (ImageView) itemView.findViewById(R.id.text4);Amount2,Amount3

        }
        public void bindView(int position) {
            // bindView() method to implement actions
        }

    }
    public class NormalViewHolder extends MyViewHolder {
        public NormalViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Do whatever you want on clicking the normal items
                }
            });
        }
    }



    public class FooterViewHolder extends MyViewHolder {
        public FooterViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Do whatever you want on clicking the item
                }
            });
        }
    }
    public DealerDeliveryHistoryadapter(Context context, List<Datum> data) {
        this.context = context;
        this.HistoryList = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
       /* View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_dealer_delivery, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;*/
        View view;
        if (viewType == FOOTER_VIEW) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.footerview_dealer_delivery_history, parent, false);

            return new FooterViewHolder(view);
        }

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_dealer_delivery, parent, false);

        return new NormalViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        try {


            if (holder instanceof NormalViewHolder) {
                NormalViewHolder vh = (NormalViewHolder) holder;
                vh.bindView(listPosition);


                TextView SNo = holder.SNo;
                TextView Text1 = holder.Text1;
                TextView Text2 = holder.Text2;
                TextView Text3 = holder.Text3;
                TextView Text4 = holder.Text4;
                TextView Text5 = holder.Text5;
                TextView Text6 = holder.Text6;
                TextView Text7 = holder.Text7;
                TextView Text8 = holder.Text8;

                SNo.setText(String.format("%s", (listPosition + 1)));
//                Text8.setText(StringToDate(HistoryList.get(listPosition).getAmountFor()+""));
//                Text6.setText(String.format("%s", HistoryList.get(listPosition).getCustomerName() + "\n" + HistoryList.get(listPosition).getCustomerMobile()));
//                Text6.setText(String.format("%s", HistoryList.get(listPosition).getCustomerName() + "\n" + HistoryList.get(listPosition).getCustomerCode()));
                Text1.setText(StringToDate(HistoryList.get(listPosition).getCreatedDate()));
                Text2.setText(String.format("%s", (HistoryList.get(listPosition).getRefillCans())));
                Text7.setText(String.format("%s", (HistoryList.get(listPosition).getRefillCansDeliverd())));
                if (String.format("%s", (HistoryList.get(listPosition).getEmptyCans())).equals("null") ||
                        String.format("%s", (HistoryList.get(listPosition).getEmptyCans())).equals("null")) {
                    Text3.setText("-");
                } else {
                    Text3.setText(String.format("%s", (HistoryList.get(listPosition).getEmptyCans())));
                }

//                if (String.format("%s", (HistoryList.get(listPosition).getEmptyCans())).equals("0")){
//                    HistoryList.remove(listPosition);
//                    notifyDataSetChanged();
//                }else {
//                    Text3.setText(String.format("%s", (HistoryList.get(listPosition).getEmptyCans())));
//                }

                SpannableString content = new SpannableString(String.format("%s", HistoryList.get(listPosition).getCustomerName() + "\n" + HistoryList.get(listPosition).getCustomerMobile()));
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);

                Text6.setText(content);

                Text4.setText(HistoryList.get(listPosition).getRefillStatus());
                if (String.format("%s", HistoryList.get(listPosition).getSuppliedDate()).equals("null"))
                    Text5.setText("-");
                else
                    Text5.setText(StringToDate(HistoryList.get(listPosition).getSuppliedDate()));
            } else if (holder instanceof FooterViewHolder) {

                FooterViewHolder vh = (FooterViewHolder) holder;
                vh.bindView(listPosition);
                TextView Amount = holder.Amount;
                TextView Amount2 = holder.Amount2;
                TextView Amount3 = holder.Amount3;
                totalamout();
                Amount.setText(String.format("%s", sum));
                totalamout2();
                Amount2.setText(String.format("%s", sum));
                totalamout3();
                Amount3.setText(String.format("%s", sum));


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

  /*  @Override
    public int getItemCount() {
        return HistoryList.size();
    }*/
  @Override
  public int getItemViewType(int position) {
      if (position == HistoryList.size()) {
          // This is where we'll add footer.
          return FOOTER_VIEW;
      }

      return super.getItemViewType(position);
  }
    @Override
    public int getItemCount() {
        if (HistoryList == null) {
            return 0;
        }

        if (HistoryList.size() == 0) {
            //Return 1 here to show nothing
            return 1;
        }

        // Add extra view to show the footer view
        return HistoryList.size() + 1;
    }

    private void totalamout() {
        sum=0;
        for (int j = 0; j < HistoryList.size(); j++) {
            final Datum pendingCansList = HistoryList.get(j);
            // Datum item = getItem(j);
            sum += pendingCansList.getRefillCans();
        }
        //holder.Amount.setText(String.format("%s", context.getString(R.string.RS)+" "+sum));

    }
    private void totalamout2() {
        sum=0;
        for (int j = 0; j < HistoryList.size(); j++) {
            final Datum pendingCansList = HistoryList.get(j);
            // Datum item = getItem(j);
            if(pendingCansList.getRefillCansDeliverd() != null)
            sum += pendingCansList.getRefillCansDeliverd();
        }
        //holder.Amount.setText(String.format("%s", context.getString(R.string.RS)+" "+sum));

    }
    private void totalamout3() {
        sum=0;
        for (int j = 0; j < HistoryList.size(); j++) {
            final Datum pendingCansList = HistoryList.get(j);
            // Datum item = getItem(j);
            if(pendingCansList.getEmptyCans() != null)
                sum += pendingCansList.getEmptyCans();
        }
        //holder.Amount.setText(String.format("%s", context.getString(R.string.RS)+" "+sum));

    }
    private String StringToDate(String Date)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat timeformat = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat newtimeformat = new SimpleDateFormat("hh:mm a");

        java.util.Date d = null;
        String newdate = null;
        try {
            d = sdf.parse(Date.substring(0,10));
            newdate = sdf2.format(d);
        } catch (ParseException ex) {
            Log.v("Date Exception",ex.getMessage());
        }
        Date time = null;
        String newtime = null;
        try {
            time = timeformat.parse(Date.substring(11,19));
            newtime = newtimeformat.format(time);
        } catch (ParseException ex) {
            Log.v("Date Exception",ex.getMessage());
        }


        return newdate + "\n"+newtime;
    }
}
