package adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.toyumwater.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import model.Dealer.InstallationHistory.Datum;


/**
 * Created by Systems02 on 05-Jul-17.
 */

public class InstallationHistoryadapter extends RecyclerView.Adapter<InstallationHistoryadapter.MyViewHolder> {

    private List<Datum> HistoryList;
    private Context context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView SNo,Text1,Text2,Text3,Text4,Text5,Text6,orderedcans,refilledcans;
        public ImageView Update;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.SNo = (TextView) itemView.findViewById(R.id.sno);
            this.Text1 = (TextView) itemView.findViewById(R.id.text1);
            this.Text2 = (TextView) itemView.findViewById(R.id.text2);
            this.Text3 = (TextView) itemView.findViewById(R.id.text3);
            this.Text4 = (TextView) itemView.findViewById(R.id.text4);
            this.Text5 = (TextView) itemView.findViewById(R.id.text5);

            this.Text6 = (TextView) itemView.findViewById(R.id.text6);

        }
    }

    public InstallationHistoryadapter(Context context, List<Datum> data) {
        this.context = context;
        this.HistoryList = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_installation_history, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {


        TextView SNo = holder.SNo;
        TextView Text1 = holder.Text1;
        TextView Text2 = holder.Text2;
        TextView Text3 = holder.Text3;
        TextView Text4 = holder.Text4;
        TextView Text5 = holder.Text5;
        TextView Text6 = holder.Text6;


        SNo.setText(String.format("%s", (listPosition + 1)));
        if (HistoryList.get(listPosition).getPayType().equals("Razorpay")){
            Text2.setText("Online");
        }else {
            Text2.setText(HistoryList.get(listPosition).getPayType());
        }
        Text3.setText(String.format("%s", (context.getString(R.string.RS)+" "+ HistoryList.get(listPosition).getDepositCost()+"0")));

        Text4.setText(String.format("%s", HistoryList.get(listPosition).getCansLimit()));
        Text6.setText(String.format("%s", HistoryList.get(listPosition).getSalesExecutive()));

        Text5.setText(StringToDate(HistoryList.get(listPosition).getInstalledDate()));


        final SpannableString content = new SpannableString(String.format("%s", HistoryList.get(listPosition).getCustomerName() + "\n" +HistoryList.get(listPosition).getMobile()));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        Text1.setText(content);

        Text1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel: " +HistoryList.get(listPosition).getMobile()));
                context.startActivity(callIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return HistoryList.size();
    }
    private String StringToDate(String Date)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat timeformat = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat newtimeformat = new SimpleDateFormat("hh:mm a");

        java.util.Date d = null;
        String newdate = null;
        try {
            d = sdf.parse(Date.substring(0,10));
            newdate = sdf2.format(d);
        } catch (ParseException ex) {
            Log.v("Date Exception",ex.getMessage());
        }
        Date time = null;
        String newtime = null;
        try {
            time = timeformat.parse(Date.substring(11,19));
            newtime = newtimeformat.format(time);
        } catch (ParseException ex) {
            Log.v("Date Exception",ex.getMessage());
        }


        return newdate + "\n"+newtime;
    }
}







