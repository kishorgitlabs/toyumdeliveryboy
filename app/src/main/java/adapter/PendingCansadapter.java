package adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.toyumwater.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import alertbox.Alert;
import model.Dealer.PendingcanList.Datum;
import toaster.Toasts;
import dealer.Dealer_Delivery_Confirmation_Activity;
import network.NetworkConnection;



/**
 * Created by Systems02 on 05-Jul-17.
 */

public class PendingCansadapter extends RecyclerView.Adapter<PendingCansadapter.MyViewHolder> {

    private List<Datum> PendingCansList;
    private Context context;
    private Alert alert;
    private NetworkConnection network;
    private Toasts toaster;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private AlertDialog alertDialog;
    int sum, sum2;
    private static final int FOOTER_VIEW = 1;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView SNo, Text1, Text2, Text3, Text4, Text5, Amount;
        public Button Update;
        public ImageView Map;

        public MyViewHolder(View itemView) {
            super(itemView);
            SNo = (TextView) itemView.findViewById(R.id.sno);
            Text1 = (TextView) itemView.findViewById(R.id.text1);
            Text2 = (TextView) itemView.findViewById(R.id.text2);
            Text3 = (TextView) itemView.findViewById(R.id.text3);
            Text4 = (TextView) itemView.findViewById(R.id.text4);
            Text5 = (TextView) itemView.findViewById(R.id.mobileno);
            Update = (Button) itemView.findViewById(R.id.update);
            Map = (ImageView) itemView.findViewById(R.id.map);
            Amount = (TextView) itemView.findViewById(R.id.amount);

        }

        public void bindView(int position) {
            // bindView() method to implement actions
        }

    }

    public class NormalViewHolder extends MyViewHolder {
        public NormalViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Do whatever you want on clicking the normal items
                }
            });
        }
    }


    public class FooterViewHolder extends MyViewHolder {
        public FooterViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Do whatever you want on clicking the item
                }
            });
        }
    }

    public PendingCansadapter(Context context, List<Datum> data) {
        this.context = context;
        this.PendingCansList = data;
        this.alert = new Alert(context);
        this.network = new NetworkConnection(context);
        this.toaster = new Toasts(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        //MyViewHolder myViewHolder = new MyViewHolder(view);

        if (viewType == FOOTER_VIEW) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.footerview_dealer_pending, parent, false);

            return new FooterViewHolder(view);
        }

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_pendingcanslist, parent, false);

        return new NormalViewHolder(view);

        //return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        try {


            if (holder instanceof NormalViewHolder) {
                NormalViewHolder vh = (NormalViewHolder) holder;
                vh.bindView(listPosition);

                holder.SNo.setText(String.format("%s", (listPosition + 1)));
//                holder.Text1.setText(String.format("%s\n%s", PendingCansList.get(listPosition).getCustomerName(), PendingCansList.get(listPosition).getCustomerMobile()));
//            holder.Text1.setText(String.format("%s\n%s", PendingCansList.get(listPosition).getCustomerName(), PendingCansList.get(listPosition).getCustomerCode()));
                holder.Text2.setText(PendingCansList.get(listPosition).getCustomerAddress());
                if (PendingCansList.get(listPosition).getRefillStatus().equals("Partially Delivered"))
                    holder.Text3.setText(String.format("%s", (PendingCansList.get(listPosition).getRefillCans() - PendingCansList.get(listPosition).getRefillCansDeliverd())));
                else
                    holder.Text3.setText(String.format("%s", PendingCansList.get(listPosition).getRefillCans()));
                holder.Text4.setText(StringToDate(PendingCansList.get(listPosition).getCreatedDate()));
                holder.Map.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String map = "http://maps.google.co.in/maps?q=" + PendingCansList.get(listPosition).getLatitude() + "," + PendingCansList.get(listPosition).getLongitude();
                        Log.v("mapURL", map);
                        Intent gotomap = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
                        context.startActivity(gotomap);
                    }
                });

                SpannableString content = new SpannableString(String.format("%s\n%s", PendingCansList.get(listPosition).getCustomerName(), PendingCansList.get(listPosition).getCustomerMobile()));
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);


                holder.Text1.setText(content);

                holder.Text1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        callIntent.setData(Uri.parse("tel: "+ PendingCansList.get(listPosition).getCustomerMobile()));
                        context.startActivity(callIntent);
                    }
                });


                holder.Update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (PendingCansList.get(listPosition).getRefillStatus().equals("Partially Delivered")) {
                            Intent a = new Intent(context, Dealer_Delivery_Confirmation_Activity.class)
                                    .putExtra("id", PendingCansList.get(listPosition).getId())
                                    .putExtra("code", PendingCansList.get(listPosition).getCustomerMobile())
                                    .putExtra("name", PendingCansList.get(listPosition).getCustomerName())
                                    .putExtra("customerAddress", PendingCansList.get(listPosition).getCustomerAddress())
                                    .putExtra("refill", PendingCansList.get(listPosition).getRefillCans() - PendingCansList.get(listPosition).getRefillCansDeliverd())
                                    .putExtra("qrcode", "No");

                            //a.putExtra("Serial id",barcode.displayValue);

                            context.startActivity(a);
//                            alertDialog.dismiss();
                            //holder.Text3.setText(String.format("%s", (PendingCansList.get(listPosition).getRefillCans() - PendingCansList.get(listPosition).getRefillCansDeliverd())));
                        } else {
                            //holder.Text3.setText(String.format("%s", PendingCansList.get(listPosition).getRefillCans()));
                            Intent a = new Intent(context, Dealer_Delivery_Confirmation_Activity.class)
                                    .putExtra("id", PendingCansList.get(listPosition).getId())
                                    .putExtra("code", PendingCansList.get(listPosition).getCustomerMobile())
                                    .putExtra("name", PendingCansList.get(listPosition).getCustomerName())
                                    .putExtra("customerAddress", PendingCansList.get(listPosition).getCustomerAddress())
                                    .putExtra("refill", PendingCansList.get(listPosition).getRefillCans())
                                    .putExtra("qrcode", "No");

                            //a.putExtra("Serial id",barcode.displayValue);

                            context.startActivity(a);
//                            alertDialog.dismiss();
                        }


                        /*myshare = context.getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);

                        alertDialog = new AlertDialog.Builder(context).create();
                        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.select_options, null);
                        alertDialog.setView(dialogView);

                        Button Cancel = (Button) dialogView.findViewById(R.id.cancel);
                        Button Conrirm = (Button) dialogView.findViewById(R.id.confirm);
                        Button ConrirmQR = (Button) dialogView.findViewById(R.id.confirmqr);

                        Cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();

                            }
                        });
                   *//* Conrirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {


                            alertDialog.dismiss();

                        }
                    });*//*
                        ConrirmQR.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Intent a = new Intent(context, Scanning_Activity.class).putExtra("position", listPosition);
                                ((Activity) context).startActivityForResult(a, 0);
                                //((Activity) context).startActivityForResult(a,0);
                                alertDialog.dismiss();
                                //context.startActivityForResult(a,0);

                            }

                        });
                        Conrirm.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (PendingCansList.get(listPosition).getRefillStatus().equals("Partially Delivered")) {
                                    Intent a = new Intent(context, Dealer_Delivery_Confirmation_Activity.class)
                                            .putExtra("id", PendingCansList.get(listPosition).getId())
                                            .putExtra("code", PendingCansList.get(listPosition).getCustomerCode())
                                            .putExtra("name", PendingCansList.get(listPosition).getCustomerName())
                                            .putExtra("refill", PendingCansList.get(listPosition).getRefillCans() - PendingCansList.get(listPosition).getRefillCansDeliverd())
                                            .putExtra("qrcode", "No");

                                    //a.putExtra("Serial id",barcode.displayValue);

                                    context.startActivity(a);
                                    alertDialog.dismiss();
                                    //holder.Text3.setText(String.format("%s", (PendingCansList.get(listPosition).getRefillCans() - PendingCansList.get(listPosition).getRefillCansDeliverd())));
                                } else {
                                    //holder.Text3.setText(String.format("%s", PendingCansList.get(listPosition).getRefillCans()));
                                    Intent a = new Intent(context, Dealer_Delivery_Confirmation_Activity.class)
                                            .putExtra("id", PendingCansList.get(listPosition).getId())
                                            .putExtra("code", PendingCansList.get(listPosition).getCustomerCode())
                                            .putExtra("name", PendingCansList.get(listPosition).getCustomerName())
                                            .putExtra("refill", PendingCansList.get(listPosition).getRefillCans())
                                            .putExtra("qrcode", "No");

                                    //a.putExtra("Serial id",barcode.displayValue);

                                    context.startActivity(a);
                                    alertDialog.dismiss();
                                }

                            }
                        });


                        alertDialog.show();*/

                    }
                });
            } else if (holder instanceof FooterViewHolder) {

                FooterViewHolder vh = (FooterViewHolder) holder;
                vh.bindView(listPosition);
                totalamout();
                holder.Amount.setText(String.format("%s", sum - sum2));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* @Override
    public int getItemCount() {
        return PendingCansList.size();

    }*/

    @Override
    public int getItemViewType(int position) {
        if (position == PendingCansList.size()) {
            // This is where we'll add footer.
            return FOOTER_VIEW;
        }

        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        if (PendingCansList == null) {
            return 0;
        }

        if (PendingCansList.size() == 0) {
            //Return 1 here to show nothing
            return 1;
        }

        // Add extra view to show the footer view
        return PendingCansList.size() + 1;
    }

    private void totalamout() {
        sum = 0;
        sum2 = 0;
        for (int j = 0; j < PendingCansList.size(); j++) {
            final Datum pendingCansList = PendingCansList.get(j);
            // Datum item = getItem(j);
            sum += pendingCansList.getRefillCans();
            if (!String.format("%s", pendingCansList.getRefillCansDeliverd()).equals("null"))
                sum2 += pendingCansList.getRefillCansDeliverd();

            //if(PendingCansList.get(listPosition).getRefillStatus().equals("Partially Delivered"))
        }
        //holder.Amount.setText(String.format("%s", context.getString(R.string.RS)+" "+sum));
    }

    private String StringToDate(String Date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat timeformat = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat newtimeformat = new SimpleDateFormat("hh:mm a");

        java.util.Date d = null;
        String newdate = null;
        try {
            d = sdf.parse(Date.substring(0, 10));
            newdate = sdf2.format(d);
        } catch (ParseException ex) {
            Log.v("Date Exception", ex.getMessage());
        }
        Date time = null;
        String newtime = null;
        try {
            time = timeformat.parse(Date.substring(11, 19));
            newtime = newtimeformat.format(time);
        } catch (ParseException ex) {
            Log.v("Date Exception", ex.getMessage());
        }


        return newdate + "\n" + newtime;
    }


}