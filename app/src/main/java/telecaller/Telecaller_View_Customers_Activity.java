package telecaller;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import java.util.ArrayList;
import java.util.List;

import apiservice.APIService;
import alertbox.Alert;
import alertbox.AlertDialogue;
import logout.logout;

import model.Telecaller.GeneralCustomerList.Datum;
import model.Telecaller.GeneralCustomerList.GeneralList;
import retroclient.RetroClient;
import sharedpreference.Shared;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Telecaller_View_Customers_Activity extends AppCompatActivity {
    String Fliter,Alphabet,Start,End,S_Result;
    TextView Result;
    Alert alert = new Alert(this);
    NetworkConnection network = new NetworkConnection(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private RecyclerView recyclerView;
    private List<Datum> GeneralList;
    private HorizontalScrollView Horizontalscrollview;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_telecaller_view_customers);

        Result = (TextView) findViewById(R.id.result);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        Horizontalscrollview = (HorizontalScrollView) findViewById(R.id.HorizontalScrollView);
        GeneralList = new ArrayList<>();
        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();

        Fliter = getIntent().getStringExtra("filtertype");
        Alphabet = getIntent().getStringExtra("alpha");
        End = getIntent().getStringExtra("end");
        S_Result = getIntent().getStringExtra("result");

        Start = getIntent().getStringExtra("start");
        if(Start.equals(""))
            Start = "0";
        End = getIntent().getStringExtra("end");
        if(End.equals(""))
            End = "0";

        Result.setText(S_Result);
        if(network.CheckInternet())
        {
            Get_GeneralList();
        }else{
            alert.showAlertbox(getString(R.string.no_network));
        }
        /*if(Fliter.equals("name"))
        {
            Toast.makeText(this, Alphabet + "\n"+Start + "\n" + End, Toast.LENGTH_SHORT).show();

        }else {
            Toast.makeText(this, Alphabet + "\n"+Start + "\n" + End, Toast.LENGTH_SHORT).show();


        }*/

    }
    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }
    public void Log_Out(View view) {
        new logout(this).log_out();
    }

    public void Popup_Menu(View view) {

    }
    private void Get_GeneralList(){

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        APIService service = RetroClient.getApiService();

        Call<GeneralList> call = service.GENERAL_LIST_CALL(
                Fliter/*,
                Alphabet,
                Integer.parseInt(Start),
                Integer.parseInt(End)*/

        );
        call.enqueue(new Callback<GeneralList>() {
            @Override
            public void onResponse(Call<GeneralList> call, Response<GeneralList> response) {
                progressDialog.dismiss();
                switch (response.body().getResult()) {
                    case "Success":
                    {
                        GeneralList = response.body().getData();
                     //   GeneralListadapter mAdapter = new GeneralListadapter(Telecaller_View_Customers_Activity.this, GeneralList);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                       // recyclerView.setAdapter(mAdapter);
                        break;
                    }
                    case "No data":
                    {
                        //alert.showAlertbox("No Installations found");
                        AlertDialogue alertnew = new AlertDialogue(Telecaller_View_Customers_Activity.this);
                        Horizontalscrollview.setVisibility(View.GONE);
                        alertnew.showAlertbox("Dear Dealer, your Customer list is empty");
                        alertnew.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                finish();
                            }
                        });
                        //alert.
                        //Body_layout.setVisibility(View.GONE);
                        break;
                    }
                    default: {

                        alert.showAlertbox(getString(R.string.connection_slow));
                        Horizontalscrollview.setVisibility(View.GONE);
                        //Body_layout.setVisibility(View.GONE);
                        break;
                    }
                }
            }

            @Override
            public void onFailure(Call<GeneralList> call, Throwable t) {
                progressDialog.dismiss();
                alert.showAlertbox(getString(R.string.connection_slow));
                Horizontalscrollview.setVisibility(View.GONE);

            }
        });

    }

}
