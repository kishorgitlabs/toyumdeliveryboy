package telecaller;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brainmagic.toyumwater.Create_Customer_Activity;
import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import customer.Customer_Changepassword_Activity;
import customer.Customer_Complaints_Activity;
import customer.Customer_Order_History_Activity;
import customer.Customer_Profile_Activity;
import customer.Customer_Welcome_Activity;
import logout.logout;
import sharedpreference.Shared;

public class Telecaller_Home_Activity extends AppCompatActivity {
    @BindView(R.id.header_layout)
    LinearLayout Headerlayout;
    @BindView(R.id.name)
    TextView Name;
    private SharedPreferences myshare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_telecaller_home);
        ButterKnife.bind(this);
        TextView header_title = Headerlayout.findViewById(R.id.header_title);
        header_title.setText("Telecaller");

        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        Name.setText(myshare.getString(Shared.K_Name, ""));



    }
    public void General_Numbers(View view) {
        startActivity(new Intent(Telecaller_Home_Activity.this,Telecaller_General_Activity.class));
    }

    public void Follow_Numbers(View view) {
        startActivity(new Intent(Telecaller_Home_Activity.this,Telecaller_Followup_Activity.class));
    }

    public void Create_Customer(View view) {
        startActivity(new Intent(Telecaller_Home_Activity.this,Create_Customer_Activity.class));
    }

    public void Change_Password(View view) {
        startActivity(new Intent(Telecaller_Home_Activity.this,Telecaller_Changepassword_Activity.class));
    }

    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {
        onBackPressed();
    }

    public void Log_Out(View view) {
        new logout(this).log_out();
    }

    public void Popup_Menu(View view) {
     /*   PopupMenu popupMenu = new PopupMenu(Telecaller_Home_Activity.this, view);
        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu pop) {
                // TODO Auto-generated method stub
                pop.dismiss();
            }
        });
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub
                switch (item.getItemId()) {
                    case R.id.c_history:
                        Intent about = new Intent(getApplicationContext(), Customer_Order_History_Activity.class);
                        startActivity(about);
                        return true;
                    case R.id.c_customer:
                        Intent services = new Intent(getApplicationContext(), Customer_Welcome_Activity.class);
                        startActivity(services);
                        return true;
                    case R.id.c_profile:
                        Intent products = new Intent(getApplicationContext(), Customer_Profile_Activity.class);
                        startActivity(products);
                        return true;
                    case R.id.c_password:
                        Intent login = new Intent(getApplicationContext(), Customer_Changepassword_Activity.class);
                        startActivity(login);
                        return true;
                    case R.id.c_suggestions:
                        Intent suggestion = new Intent(getApplicationContext(), Customer_Complaints_Activity.class);
                        startActivity(suggestion);
                        return true;
                }
                return false;
            }
        });
        popupMenu.inflate(R.menu.popupmenu_customer);
        popupMenu.getMenu().findItem(R.id.c_password).setVisible(false);
        popupMenu.show();*/
    }

}
