package customer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import apiservice.APIService;
import alertbox.Alert;
import logout.logout;
import model.Customer.Suggeation_Complaint.suggestionorcomplaint;
import retroclient.RetroClient;
import sharedpreference.Shared;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Customer_Complaints_Activity extends AppCompatActivity {

    TextView Customer_Name, Customer_Code;
    EditText Suggestion;
    Alert alert = new Alert(this);
    NetworkConnection network = new NetworkConnection(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_complaints);


        Customer_Name = (TextView) findViewById(R.id.name);
        Customer_Code = (TextView) findViewById(R.id.code);
        Suggestion = (EditText) findViewById(R.id.suggestion);

        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();

        Customer_Name.setText(myshare.getString(Shared.K_Name, ""));
        Customer_Code.setText(myshare.getString(Shared.K_Code, ""));


    }

    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }

    public void Register(View view) {

        if (!Suggestion.getText().toString().equals("")) {

            if (network.CheckInternet()) {
                Send_Suggestion();
            } else {
                alert.showAlertbox(getString(R.string.no_network));
            }
        } else {
            Suggestion.setError("Please fill the Suggestion/Complaint");
            Suggestion.setFocusable(true);
            //alert.showAlertbox("Kindly fill the Suggestoin");
        }
    }

    private void Send_Suggestion() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        APIService service = RetroClient.getApiService();
        Call<suggestionorcomplaint> call = service.Suggeation_or_Complaint(myshare.getString(Shared.K_Name, ""), myshare.getString(Shared.K_Code, ""), Suggestion.getText().toString());

        call.enqueue(new Callback<suggestionorcomplaint>() {
            @Override
            public void onResponse(Call<suggestionorcomplaint> call, Response<suggestionorcomplaint> response) {
                progressDialog.dismiss();
                switch (response.body().getResult()) {
                    case "Success":
                        alert.showAlertbox("Your Suggestion/Complaint successfully submitted; Thank you for Contacting TOYUM");
                        Suggestion.setText("");
                        Suggestion.clearFocus();
                        break;
                    case "Failure":
                        alert.showAlertbox("Error in submitting! Please try again !");
                        break;
                }
            }

            @Override
            public void onFailure(Call<suggestionorcomplaint> call, Throwable t) {
                progressDialog.dismiss();
                alert.showAlertbox(getString(R.string.connection_slow));
            }
        });


    }
    public void Log_Out(View view) {
        new logout(this).log_out();
    }


    public void Popup_Menu(View view) {
        PopupMenu popupMenu = new PopupMenu(Customer_Complaints_Activity.this, view);
        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu pop) {
                // TODO Auto-generated method stub
                pop.dismiss();
            }
        });
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub
                switch (item.getItemId()) {
                    case R.id.c_history:
                        Intent about = new Intent(getApplicationContext(), Customer_Order_History_Activity.class);
                        startActivity(about);
                        return true;
                    case R.id.c_customer:
                        Intent services = new Intent(getApplicationContext(), Customer_Welcome_Activity.class);
                        startActivity(services);
                        return true;
                    case R.id.c_profile:
                        Intent products = new Intent(getApplicationContext(),Customer_Profile_Activity.class);
                        startActivity(products);
                        return true;
                    case R.id.c_password:
                        Intent login = new Intent(getApplicationContext(), Customer_Changepassword_Activity.class);
                        startActivity(login);
                        return true;
                    case R.id.c_suggestions:
                        Intent suggestion = new Intent(getApplicationContext(), Customer_Complaints_Activity.class);
                        startActivity(suggestion);
                        return true;
                }
                return false;
            }
        });
        popupMenu.inflate(R.menu.popupmenu_customer);
        popupMenu.getMenu().findItem(R.id.c_suggestions).setVisible(false);
        popupMenu.show();
    }

}
