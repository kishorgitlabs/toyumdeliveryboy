package customer;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import apiservice.APIService;
import alertbox.Alert;
import alertbox.AlertDialogue;
import logout.logout;
import model.Customer.CustomerInfo.Customerinfo;
import model.Dealer.CustomerLocationUpdate.CustomerLocationupdate;
import retroclient.RetroClient;
import sharedpreference.Shared;
import toaster.Toasts;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import service.GPSTracker;

public class Customer_Location_Update_Activity extends AppCompatActivity {
    TextView Customer_Name,Customer_Code,Latitude,Longitude;
    Alert alert = new Alert(this);
    NetworkConnection network = new NetworkConnection(this);
    Toasts toaster = new Toasts(this);;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Customerinfo info ;
    double latitude; // latitude
    double longitude; // longitude
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_location_update);
        Customer_Name = (TextView)findViewById(R.id.name);
        Customer_Code = (TextView)findViewById(R.id.code);
        Latitude = (TextView) findViewById(R.id.latitude);
        Longitude = (TextView) findViewById(R.id.longitude);

        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);


        if(network.CheckInternet())
        {
            Get_CustomerInfo();
        }else{
            alert.showAlertbox(getString(R.string.no_network));
        }
    }
    @Override
    protected void onRestart() {
        recreate();
        super.onRestart();
    }

    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }
    public void Log_Out(View view) {
        new logout(this).log_out();
    }

    public void Popup_Menu(View view) {

    }
    public void Get_Coordinates(View view) {
        GPSTracker gps = new GPSTracker(Customer_Location_Update_Activity.this);

        // check if GPS enabled
        if(gps.canGetLocation()){

            latitude = gps.getLatitude();
            longitude = gps.getLongitude();

            Latitude.setText(String.format("%s",latitude));
            Longitude.setText(String.format("%s",longitude));

            // \n is for new line
            //Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
        }else{
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
    }

    public void Update_Coordinates(View view) {
        if (Latitude.getText().toString().equals("")) {
            toaster.ShowErrorToast("Kindly Get Coordinates !");
        } else if (Longitude.getText().toString().equals("")) {
            toaster.ShowErrorToast("Kindly Get Coordinates !");
        } else if (network.CheckInternet()) {
            UpdateInstallation();
        } else {
            alert.showAlertbox(getString(R.string.no_network));
        }
    }

    public void Locate(View view) {
        String map = "http://maps.google.co.in/maps?q="+info.getData().getLatitude()+","+info.getData().getLangitude();
        Log.v("mapURL",map);
        Intent gotomap = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
        startActivity(gotomap);
    }

    private void UpdateInstallation() {
        final ProgressDialog progressDialog = new ProgressDialog(Customer_Location_Update_Activity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        APIService service = RetroClient.getApiService();

        Call<CustomerLocationupdate> call = service.CUSTOMER_LOCATIONUPDATE_CALL(myshare.getString(Shared.K_Code,""),Latitude.getText().toString(),Longitude.getText().toString());
        call.enqueue(new Callback<CustomerLocationupdate>() {
            @Override
            public void onResponse(Call<CustomerLocationupdate> call, Response<CustomerLocationupdate> response) {
                progressDialog.dismiss();
                switch (response.body().getResult()) {
                    case "Success": {

                        AlertDialogue alertnew = new AlertDialogue(Customer_Location_Update_Activity.this);
                        alertnew.showAlertbox("Customer location updated Successfully");
                        alertnew.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                recreate();
                            }
                        });
                        //finish();
                        break;
                    }
                    case "Failure": {

                        alert.showAlertbox("Updated failed, Please try again !");

                        recreate();
                        break;
                    }
                    default: {
                        alert.showAlertbox(getString(R.string.connection_slow));
                        break;
                    }
                }
            }

            @Override
            public void onFailure(Call<CustomerLocationupdate> call, Throwable t) {

                progressDialog.dismiss();
                alert.showAlertbox(getString(R.string.connection_slow));

            }
        });
    }





    private void Get_CustomerInfo() {

        try {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            APIService service = RetroClient.getApiService();
            Call<Customerinfo> call = service.CUSTOMERINFO_CALL(myshare.getString(Shared.K_Code,""));
            call.enqueue(new Callback<Customerinfo>() {
                @Override
                public void onResponse(Call<Customerinfo> call, Response<Customerinfo> response) {
                    progressDialog.dismiss();

                    switch (response.body().getResult()) {

                        case "Success":

                        {
                            info = response.body();
                            Customer_Name.setText(info.getData().getCustomerName());
                            Customer_Code.setText(info.getData().getCustomerCode());

                            break;
                        }
                        case "No data":
                        {
                            alert.showAlertbox(getString(R.string.connection_slow));
                            break;
                        }
                        default: {
                            alert.showAlertbox(getString(R.string.connection_slow));
                            break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<Customerinfo> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
