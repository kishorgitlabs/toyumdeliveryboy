package customer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import apiservice.APIService;
import alertbox.Alert;
import logout.logout;
import model.ChangePassword.changepassword;
import retroclient.RetroClient;
import sharedpreference.Shared;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Customer_Changepassword_Activity extends AppCompatActivity {

    TextView Customer_Name,Customer_Code;
    EditText CurrentPassword,NewPassword,ConfirmPassword;
    Alert alert = new Alert(this);
    NetworkConnection network = new NetworkConnection(this);

    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_changepassword);


        Customer_Name = (TextView)findViewById(R.id.name);
        Customer_Code = (TextView)findViewById(R.id.code);

        CurrentPassword=   (EditText) findViewById(R.id.current_password);
        NewPassword=   (EditText) findViewById(R.id.new_password);
        ConfirmPassword=   (EditText) findViewById(R.id.confirm_password);

        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();

        Customer_Name.setText(myshare.getString(Shared.K_Name,""));
        Customer_Code.setText(myshare.getString(Shared.K_Code,""));
    }

    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }

    public void Log_Out(View view) {
        new logout(this).log_out();
    }

    public void Change_Password(View view) {
        if (CurrentPassword.getText().toString().length() == 0) {
            CurrentPassword.setFocusable(true);
            CurrentPassword.setError("Enter Password");
        } else if (NewPassword.getText().toString().length() == 0) {
            NewPassword.setFocusable(true);
            NewPassword.setError("Enter Password");
        } else if (ConfirmPassword.getText().toString().length() == 0) {
            ConfirmPassword.setFocusable(true);
            ConfirmPassword.setError("Enter Password");
        }else if (NewPassword.getText().toString().length() < 8) {
            NewPassword.setFocusable(true);
            NewPassword.setError("Password should be minimum 8 characters");
        } else if(!NewPassword.getText().toString().equals(ConfirmPassword.getText().toString()))
        {
            ConfirmPassword.setFocusable(true);
            ConfirmPassword.setError("New password & Confirm password doesn't match");
        }else
        {
            if (network.CheckInternet()) {
                Change_Password();
            } else {
                alert.showAlertbox(getString(R.string.no_network));
            }
        }


    }

    private void Change_Password() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        APIService service = RetroClient.getApiService();
        Call<changepassword> call = service.Change_Password (myshare.getLong(Shared.K_Id, 0), CurrentPassword.getText().toString(), NewPassword.getText().toString());

        call.enqueue(new Callback<changepassword>() {
            @Override
            public void onResponse(Call<changepassword> call, Response<changepassword> response) {
                progressDialog.dismiss();
                switch (response.body().getResult()) {
                    case "Success":
                        alert.showAlertbox("Dear Customer, your new password " + response.body().getData().getNewPassword() + " updated successfully !");
                        CurrentPassword.setText("");
                        NewPassword.setText("");
                        ConfirmPassword.setText("");
                        break;
                    case "IncorrectPassword":
                        alert.showAlertbox("Your Current password is Incorrect! Kindly enter valid password");

                        break;
                    default:
                        alert.showAlertbox(getString(R.string.connection_slow));
                        break;

                }
            }

            @Override
            public void onFailure(Call<changepassword> call, Throwable t) {
                progressDialog.dismiss();
                alert.showAlertbox(getString(R.string.connection_slow));
            }
        });


    }


    public void Popup_Menu(View view) {
        PopupMenu popupMenu = new PopupMenu(Customer_Changepassword_Activity.this, view);
        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu pop) {
                // TODO Auto-generated method stub
                pop.dismiss();
            }
        });
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub
                switch (item.getItemId()) {
                    case R.id.c_history:
                        Intent about = new Intent(getApplicationContext(), Customer_Order_History_Activity.class);
                        startActivity(about);
                        return true;
                    case R.id.c_customer:
                        Intent services = new Intent(getApplicationContext(), Customer_Welcome_Activity.class);
                        startActivity(services);
                        return true;
                    case R.id.c_profile:
                        Intent products = new Intent(getApplicationContext(),Customer_Profile_Activity.class);
                        startActivity(products);
                        return true;
                    case R.id.c_password:
                        Intent login = new Intent(getApplicationContext(), Customer_Changepassword_Activity.class);
                        startActivity(login);
                        return true;
                    case R.id.c_suggestions:
                        Intent suggestion = new Intent(getApplicationContext(), Customer_Complaints_Activity.class);
                        startActivity(suggestion);
                        return true;
                }
                return false;
            }
        });
        popupMenu.inflate(R.menu.popupmenu_customer);
        popupMenu.getMenu().findItem(R.id.c_password).setVisible(false);
        popupMenu.show();
    }
}
