package customer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import com.brainmagic.toyumwater.Home_Activity;
import com.brainmagic.toyumwater.R;

import logout.logout;

public class Customer_Delivery extends AppCompatActivity {
    Button ConfirmQR,ConfirmManually;
    ImageView Back,Home;
    TextView Customer_Name,Customer_Code;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_delivery);
        ConfirmQR = (Button)findViewById(R.id.confirmqr);
        ConfirmManually = (Button)findViewById(R.id.confirm);
        Back = (ImageView)findViewById(R.id.back);
        Home = (ImageView)findViewById(R.id.home);
        Customer_Name = (TextView)findViewById(R.id.name);
        Customer_Code = (TextView)findViewById(R.id.code);

    }

    public void Confirm_QRcode(View view) {
startActivity(new Intent(Customer_Delivery.this,Customer_Delivery_Confirmation_Activity.class));
    }

    public void Confirm_Manually(View view) {
        startActivity(new Intent(Customer_Delivery.this,Customer_Delivery_Confirmation_Activity.class));

    }
    public void Home(View view) {
        Intent a = new Intent(getApplicationContext(), Home_Activity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void Back(View view) {

        onBackPressed();
    }
    public void Log_Out(View view) {
        new logout(this).log_out();
    }

    public void Popup_Menu(View view) {
    }
}
